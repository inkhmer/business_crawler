require('./libs/bootstrap');
var winston = require('./libs/winston'),
    fse = require('fs-extra'),
    shuffle = require('knuth-shuffle').knuthShuffle,
    request = require('request'),
    async = require('async'),
    csvWriter = require('csv-write-stream'),
    path = require('path'),
    trycatch = require('trycatch'),
    checkProxy = require('check-proxy').check,
    _ = require('lodash');

var cloudscraper = require('./libs/cloudscraper');
var json = {
    "defaultTimeout": 5000,
    "port": 8000,
    "repairTime": 600,
    "blockTimeout": 3600,
    "logLevel": "INFO",
    "graceTime": 150,
    "logging": {
        "appenders": [
            { "type": "console" }
        ],
        "replaceConsole": true
    },
    "proxies": []
}

// async.eachLimit(_.range(1, 10),1,  function (i, nextFn) {
//     request('https://gimmeproxy.com/api/getProxy?get=true', function (err, response, body) {
//         if (err) {
//             winston.error(err)
//             return nextFn(err);
//         }

//         if (!body) return nextFn(new Error('No Data'));

//         var bodyJson = JSON.parse(body);
//         if (bodyJson.curl.indexOf('http') > -1) {
//             winston.info(bodyJson.curl);
//             json.proxies.push(bodyJson.curl);
//         }
//         nextFn();
//     });
// }, function (err, result) {
//     json.proxies = _.compact(json.proxies);
//     if (json.proxies.length > 0) {
//         fse.writeJsonSync(__dirname + '/node_modules/proxy-rotator/config.json', json)
//         fse.removeSync(__dirname + '/node_modules/proxy-rotator/.proxies.tmp');

//         winston.info('Proxy has been updated');
//     } else {
//         winston.info('Proxy has no ips')

//     }
//     process.exit(0);
// })

// cloudscraper.get('http://www.cambodiayp.com/', function(error, response, body) {
//   if (error) {
//     console.log('Error occurred');
//   } else {
//     console.log(body, response);
//   }
// });

var ProxyLists = require('proxy-lists');

var options = {
    countries: null,
    protocols: ['http', 'https'],
    anonymityLevels: ['anonymous', 'elite'],
    // sourcesWhiteList: ['bitproxies', 'kingproxies', 'proxydb', 'incloak'],
    // sourcesWhiteList: ['proxydb', 'incloak'],
    // sourcesWhiteList: ['gatherproxy', 'proxylisten'],
    // sourcesWhiteList: ['freeproxylists'],
    sourcesWhiteList: ['incloak'],
    sourcesBlackList: null,
    series: false,
    ipTypes: ['ipv4'],
    bitproxies: {
        apiKey: '9ZzEro06lTY1B5ah2Vqye4LgWNehFfGZ'
    },
    kingproxies: {
        apiKey: 'a3af49e3df9c425ebc224001acb1e0'
    }
};

// `gettingProxies` is an event emitter object.
var gettingProxies = ProxyLists.getProxies(options);
winston.info('Getting Proxies...');

gettingProxies.on('data', function (proxies, source) {
    // winston.info('Got Proxies From ', source);
    proxies = proxies.map(function (x) {
        return x.protocols[0] + "://" + x.ipAddress + ":" + x.port;
    });

    proxies = proxies.filter(function (x) {
        return x.indexOf('XXX.XXX') == -1
    });

    json.proxies = _.uniqBy(_.compact(json.proxies.concat(proxies)));
});

gettingProxies.on('error', function (error) {
    winston.error(error);
});

var _csvWriter = null;
if (!_csvWriter) {
    _csvWriter = csvWriter({ headers: ['url'] });

    var fileName = path.resolve("data/proxy-urls.csv");
    _csvWriter.pipe(fse.createWriteStream(fileName))
}

gettingProxies.once('end', function () {
    
    if (json.proxies.length > 0) {
        var validProxies = [];
        var userAgents = fse.readJsonSync(__dirname + '/config/allagents.json');

        winston.info('Total Proxies: ', json.proxies.length);

        async.eachLimit(json.proxies, 10, function (proxyUrl, nextProxyFn) {
            var userAgent = _.first(shuffle(userAgents));

            var requestOptions = {
                method: 'GET',
                url: 'http://www.tripadvisor.com',
                proxy: proxyUrl,
                headers: {
                    'User-Agent': userAgent
                }
            };

            winston.info('Checking proxy url: ', proxyUrl);
            trycatch(function () {

                // checkProxy({
                //     testHost: 'localhost', // put your ping server url here 
                //     proxyIP: proxyUrl, // proxy ip to test 
                //     proxyPort: 80, // proxy port to test 
                //     localIP: '185.103.27.23', // local machine IP address to test 
                //     connectTimeout: 6, // curl connect timeout, sec 
                //     timeout: 10, // curl timeout, sec 
                //     websites: [
                //         {
                //             name: 'TripAdvisor',
                //             url: 'http://www.tripadvisor.com',
                //             regex: /example/gim, // expected result - regex 

                //         }
                //     ]
                // }).then(function (res) {
                //     winston.info(proxyUrl, ' valid');
                //     validProxies.push(proxyUrl);
                //     _csvWriter.write({ url: proxyUrl });
                //     nextProxyFn();
                // }, function (err) {
                //     console.log('proxy rejected', err);
                //     nextProxyFn();
                // });




                request(requestOptions, function (err, response, body) {
                    if (err) {
                        winston.error(err)
                        return nextProxyFn();
                    } else {
                        if (!body || body.indexOf('id="suspended"') > -1) {
                            winston.warn(proxyUrl, ' invalid');
                            return nextProxyFn();
                        }
                        winston.info(proxyUrl, ' valid');
                        validProxies.push(proxyUrl);
                        _csvWriter.write({ url: proxyUrl });

                        nextProxyFn();
                    }
                });
            }, nextProxyFn)

        }, function () {
            winston.info('Valid Proxies Count: ', validProxies);

            json.proxies = validProxies;
            if (json.proxies.length > 0) {
                fse.writeJsonSync(__dirname + '/libs/proxy-rotator/config.json', json);
                fse.removeSync(__dirname + '/libs/proxy-rotator/.proxies.tmp');

                winston.info('Proxy has been updated');
                process.exit();

            } else {
                winston.info('Proxy has no ips')
                process.exit();
            }
        })
    } else {
        winston.info('Proxy has no ips');

    }
});
