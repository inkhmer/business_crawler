'use strict';
require('./libs/bootstrap');

var winston = require('./libs/winston'),
    async = require('async'),
    path = require('path'),
    csvWriter = require('csv-write-stream'),
    fse = require('fs-extra'),
    moment = require('moment'),
    csv = require("fast-csv"),
    _ = require('lodash'),
    glob = require("glob"),
    NodeGeocoder = require('node-geocoder'),
    cachePath = path.join(__dirname, 'cache', 'cache-memoize'),
    YellowPagesAu = require('./providers/yellowpages.com.au'),
    memoize = require('memoize-fs')({ cachePath: cachePath });

const fileTimestampFormat = "YYYY-MM-DD_hhmmss";
const trueLocalFileNameFormat = 'data/categories-truelocal-';
const yellowPagesAuFileNameFormat = 'data/categories-yellowpages-au-';
const sourceFilePath = path.resolve('./data/truelocal.com.au/categories.json')
var _trueLocalCsvWriter = csvWriter({ headers: ['type', 'enCategory', 'enSubCategory', 'enKeywords'] });
var processingTrueLocalFilePath = path.resolve(_.template("{{fileName}}{{timestamp}}.csv")({ fileName: trueLocalFileNameFormat, timestamp: moment().format(fileTimestampFormat) }));
_trueLocalCsvWriter.pipe(fse.createWriteStream(processingTrueLocalFilePath))

function cleanFiles(opt, fileNameFormat, retentionNumber) {
    retentionNumber = retentionNumber || 1;

    glob(path.resolve(_.template("{{fileName}}*.csv")({fileName: fileNameFormat})), {}, function (err, files) {
        files = _.sortBy(files, function (file) {
            var fileName = path.basename(file);
            return moment(fileName, fileTimestampFormat);
        });

        var toDeleteCount = files.length - retentionNumber;

        if (toDeleteCount > 0) {
            var toDeleteFiles = _.take(files, toDeleteCount);

            _.each(toDeleteFiles, function (file) {
                fse.removeSync(file);
            });
        }
    });
}

var categoryJson = fse.readJsonSync(sourceFilePath);

_.each(categoryJson.data.parentCategory, function(category) {
    _trueLocalCsvWriter.write({
        type: 'category',
        enCategory: category.description
    });

    _.each(category.subCategories.subCategory, function(subCategory) {
        _trueLocalCsvWriter.write({
            type: 'subCategory',
            enCategory: category.description,
            enSubCategory: subCategory.description,
            enKeywords: subCategory.keywordsSummary
        })
    });
});

_trueLocalCsvWriter.end();
cleanFiles(null, trueLocalFileNameFormat, 1);
winston.info('File has been saved to: %s', processingTrueLocalFilePath);


var _yellowPagesAuCsvWriter = csvWriter({ headers: ['type', 'enCategory', 'enSubCategory', 'enKeywords'] });
var processingYellowPagesAuFilePath = path.resolve(_.template("{{fileName}}{{timestamp}}.csv")({ fileName: yellowPagesAuFileNameFormat, timestamp: moment().format(fileTimestampFormat) }));
_yellowPagesAuCsvWriter.pipe(fse.createWriteStream(processingYellowPagesAuFilePath))

var yellowPagesAu = new YellowPagesAu(_yellowPagesAuCsvWriter)

yellowPagesAu.getAllFilterings(function(err, filterings) {
    if(err) {
        return winston.error(err);
    }
    fse.writeJsonSync('./data/category-yp.json', filterings)
    winston.info(filterings)
})

_yellowPagesAuCsvWriter.end();
cleanFiles(null, yellowPagesAuFileNameFormat, 1);
winston.info('File has been saved to: %s', processingYellowPagesAuFilePath);