enum PropertyType {
    Unknown = 0,
    House = 1,
    Apartment = 2,
    Unit = 3,
    Townhouse = 4,
    Villa = 5,
    Land = 6,
    Acreage = 7,
    Rural = 8,
    BlockOfUnits = 9
}

export = PropertyType;