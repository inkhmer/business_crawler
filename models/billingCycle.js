"use strict";
var BillingCycle;
(function (BillingCycle) {
    BillingCycle[BillingCycle["NotAvailable"] = 0] = "NotAvailable";
    BillingCycle[BillingCycle["Month"] = 1] = "Month";
    BillingCycle[BillingCycle["Annual"] = 2] = "Annual";
})(BillingCycle || (BillingCycle = {}));
module.exports = BillingCycle;
//# sourceMappingURL=billingCycle.js.map