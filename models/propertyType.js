"use strict";
var PropertyType;
(function (PropertyType) {
    PropertyType[PropertyType["Unknown"] = 0] = "Unknown";
    PropertyType[PropertyType["House"] = 1] = "House";
    PropertyType[PropertyType["Apartment"] = 2] = "Apartment";
    PropertyType[PropertyType["Unit"] = 3] = "Unit";
    PropertyType[PropertyType["Townhouse"] = 4] = "Townhouse";
    PropertyType[PropertyType["Villa"] = 5] = "Villa";
    PropertyType[PropertyType["Land"] = 6] = "Land";
    PropertyType[PropertyType["Acreage"] = 7] = "Acreage";
    PropertyType[PropertyType["Rural"] = 8] = "Rural";
    PropertyType[PropertyType["BlockOfUnits"] = 9] = "BlockOfUnits";
})(PropertyType || (PropertyType = {}));
module.exports = PropertyType;
//# sourceMappingURL=propertyType.js.map