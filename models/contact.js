"use strict";
var Contact = (function () {
    function Contact(name, phone, email) {
        this.name = name;
        this.phone = phone;
        this.email = email;
    }
    return Contact;
}());
module.exports = Contact;
//# sourceMappingURL=contact.js.map