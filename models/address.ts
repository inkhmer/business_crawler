class Address {
    street: string;
    country: string = 'Cambodia';
    sangkat: string;
    khan: string;
    provinceOrCity: string;
    lat: number;
    lng: number;
};

export = Address;