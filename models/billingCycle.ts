enum BillingCycle {
    NotAvailable = 0,
    Month = 1,
    Annual = 2
}

export = BillingCycle;