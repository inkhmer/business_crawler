class GeoLocation {
    public Latitude: number;
    public Longitude: number;

    /**
     *
     */
    constructor(latitude:number,longitude: number) {
        this.Latitude = latitude;
        this.Longitude = longitude;
    }
}

export = Geolocation;