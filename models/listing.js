"use strict";
var _ = require('lodash'), winston = require('../libs/winston'), URI = require('urijs');
var Listing = (function () {
    function Listing(url, provider) {
        this.excludedCustomProperties = [];
        this.url = url;
        this.cacheId = this.getId();
        this.provider = provider;
    }
    Listing.prototype.setCustomProperties = function (properties) {
        var self = this;
        if (_.keys(self.customProperties).length == 0) {
            var properties = _.chain(properties).map(function (x) {
                x.name = x.name.toLowerCase();
                return x;
            })
                .reject(function (x) {
                var exclude = _.filter(self.excludedCustomProperties, function (name) {
                    return x.name.indexOf(name) > -1;
                }).length > 0;
                return exclude;
            }).value();
            self.customProperties = _.zipObject(_.map(properties, 'name'), _.map(properties, 'value'));
        }
    };
    Listing.prototype.setPropertyValue = function (targetName, sourceName) {
        var self = this;
        if (self.customProperties[sourceName]) {
            _.set(this, targetName, self.customProperties[sourceName]);
            delete self.customProperties[sourceName];
        }
    };
    Listing.prototype.customProperty = function (key, value) {
        if (key == null)
            return null;
        if (arguments.length == 1) {
            return this.customProperties[key.toLowerCase()];
        }
        else {
            if (value == null) {
                delete this.customProperties[key];
            }
            else {
                this.customProperties[key] = value;
            }
        }
    };
    /*
    * Get property or next one
    */
    Listing.prototype.getCustomProperty = function () {
        var self = this;
        return _.first(_.compact(_.map(arguments, function (key) {
            return self.customProperties[key.toLowerCase()];
        })));
    };
    Listing.prototype.removeCustomProperties = function (keys) {
        var self = this;
        _.each(keys, function (key) {
            delete self.customProperties[key];
        });
    };
    // public setFeatures(provider: any, features: string[]): void {
    //     var self = this;
    //     _.each(features, function (name) {
    //         var featureNotExists = true;
    //         var feature = provider.mapper.indoorFeature.firstOrDefault(name);
    //         if (feature) self.indoorFeatures.push(feature);
    //         featureNotExists = !feature;
    //         feature = provider.mapper.outdoorFeature.firstOrDefault(name);
    //         if (feature) self.outdoorFeatures.push(feature);
    //         featureNotExists = featureNotExists && !feature;
    //         feature = provider.mapper.ecoFeature.firstOrDefault(name);
    //         if (feature) self.ecoFeatures.push(feature);
    //         featureNotExists = featureNotExists && !feature;
    //         if (featureNotExists) winston.warn('%s: %s doesn\'t exist.\n', provider.name, name);
    //     });
    // }
    Listing.prototype.getId = function () {
        if (this.url) {
            var uri = URI(this.url);
            return uri.hostname() + uri.path() + (uri.query() ? '?' : '') + uri.query();
        }
        return null;
    };
    return Listing;
}());
module.exports = Listing;
//# sourceMappingURL=listing.js.map