enum Type {
    Unknown = 0,
    ForSale = 1,
    ToRent = 2
}

export = Type;