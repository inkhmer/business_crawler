
var _ = require('lodash'),
    winston = require('../libs/winston'),
    URI = require('urijs');

class Listing {
    public provider: string;
    public url: string;
    public name: string;
    public imageUrls: string[];
    public description: string;
    public createdOn: Date;
    public idFromProvider: string;
    public customProperties: Object;
    public cv: string;
    public logo: string;
    public socialFacebook: string;
    public addressLng: string;
    public addressLat: string;
    public emails: string;
    public tags: string;
    public addressPostalCode: string;
    public addressStreet: string;
    public addressLocality: string;
    public website: string;
    public companyName: string;
    public excludedCustomProperties: string[] = [];
    public cacheId: string;

    constructor(url: string, provider: string) {
        this.url = url;
        this.cacheId = this.getId();
        this.provider = provider;
    }

    public setCustomProperties(properties: Object[]): void {
        var self = this;

        if (_.keys(self.customProperties).length == 0) {
            var properties = _.chain(properties).map(function (x) {
                x.name = x.name.toLowerCase();
                return x;
            })
                .reject(function (x) {
                    var exclude = _.filter(self.excludedCustomProperties, function (name) {
                        return x.name.indexOf(name) > -1;
                    }).length > 0;

                    return exclude;
                }).value();

            self.customProperties = _.zipObject(_.map(properties, 'name'), _.map(properties, 'value'));
        }
    }

    public setPropertyValue(targetName: string, sourceName: string): void {
        var self = this;

        if (self.customProperties[sourceName]) {
            _.set(this, targetName, self.customProperties[sourceName]);

            delete self.customProperties[sourceName];
        }
    }

    public customProperty(key: string, value: string): string {
        if (key == null) return null;
        if (arguments.length == 1) {
            return this.customProperties[key.toLowerCase()];
        } else {
            if (value == null) {
                delete this.customProperties[key];
            } else {
                this.customProperties[key] = value;
            }
        }
    }
    /*
    * Get property or next one
    */
    public getCustomProperty(): string {
        var self = this;

        return _.first(_.compact(_.map(arguments, function (key) {
            return self.customProperties[key.toLowerCase()];
        })));
    }

    public removeCustomProperties(keys: string[]) {
        var self = this;
        _.each(keys, function (key) {
            delete self.customProperties[key];
        });
    }

    // public setFeatures(provider: any, features: string[]): void {
    //     var self = this;
    //     _.each(features, function (name) {
    //         var featureNotExists = true;
    //         var feature = provider.mapper.indoorFeature.firstOrDefault(name);
    //         if (feature) self.indoorFeatures.push(feature);
    //         featureNotExists = !feature;

    //         feature = provider.mapper.outdoorFeature.firstOrDefault(name);
    //         if (feature) self.outdoorFeatures.push(feature);
    //         featureNotExists = featureNotExists && !feature;

    //         feature = provider.mapper.ecoFeature.firstOrDefault(name);
    //         if (feature) self.ecoFeatures.push(feature);
    //         featureNotExists = featureNotExists && !feature;

    //         if (featureNotExists) winston.warn('%s: %s doesn\'t exist.\n', provider.name, name);
    //     });
    // }

    public getId(): string {
        if (this.url) {
            var uri = URI(this.url);
            return uri.hostname() + uri.path() + (uri.query() ? '?' : '') + uri.query();
        }
        return null;
    }
}

export = Listing;