"use strict";
var Type;
(function (Type) {
    Type[Type["Unknown"] = 0] = "Unknown";
    Type[Type["ForSale"] = 1] = "ForSale";
    Type[Type["ToRent"] = 2] = "ToRent";
})(Type || (Type = {}));
module.exports = Type;
//# sourceMappingURL=type.js.map