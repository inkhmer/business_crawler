'use strict';
require('./libs/bootstrap');
var winston = require('./libs/winston'), async = require('async'), path = require('path'), csvWriter = require('csv-write-stream'), fse = require('fs-extra'), moment = require('moment'), _ = require('lodash'), glob = require("glob"), CambodiaPostProvider = require('./providers/cambodiapostcode');
var fileTimestampFormat = "YYYY-MM-DD_hhmmss";
var _csvWriter = csvWriter({ headers: ['id', 'url', 'type', 'enProvince', 'khProvince', 'enDistrict', 'khDistrict', 'enCommune', 'khCommune', 'postcode', 'latitude', 'longitude'] });
function cleanFiles(opt, fileFormat, retentionNumber) {
    retentionNumber = retentionNumber || 2;
    fileFormat = fileFormat || fileTimestampFormat;
    glob(path.resolve("data/cambodia-postcode-*.csv"), {}, function (err, files) {
        files = _.sortBy(files, function (file) {
            var fileName = path.basename(file);
            return moment(fileName, fileFormat);
        });
        var toDeleteCount = files.length - retentionNumber;
        if (toDeleteCount > 0) {
            var toDeleteFiles = _.take(files, toDeleteCount);
            _.each(toDeleteFiles, function (file) {
                fse.removeSync(file);
            });
        }
    });
}
var fileName = path.resolve(_.template("data/cambodia-postcode-{{timestamp}}.csv")({ timestamp: moment().format(fileTimestampFormat) }));
_csvWriter.pipe(fse.createWriteStream(fileName));
var cambodiaPostProvider = new CambodiaPostProvider(_csvWriter);
cambodiaPostProvider.getAllCommunes(function (err, communes) {
    if (err) {
        return winston.error(err);
    }
    winston.info("Total: %s", communes.length);
    _csvWriter.end();
    cleanFiles(null, null, null);
});
//# sourceMappingURL=getCambodiaPostCode.js.map