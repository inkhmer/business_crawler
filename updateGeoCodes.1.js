'use strict';
require('./libs/bootstrap');
var winston = require('./libs/winston'), async = require('async'), path = require('path'), csvWriter = require('csv-write-stream'), fse = require('fs-extra'), moment = require('moment'), csv = require("fast-csv"), _ = require('lodash'), glob = require("glob"), NodeGeocoder = require('node-geocoder'), cachePath = path.join(__dirname, 'cache', 'cache-memoize'), memoize = require('memoize-fs')({ cachePath: cachePath });
var fileTimestampFormat = "YYYY-MM-DD_hhmmss";
var csvFile = path.resolve('./data/cambodia-postcode.csv');
var _csvWriter = csvWriter({ headers: ['id', 'url', 'type', 'enProvince', 'khProvince', 'enDistrict', 'khDistrict', 'enCommune', 'khCommune', 'postcode', 'latitude', 'longitude'] });
var fileName = path.resolve(_.template("data/cambodia-postcode-latlng-{{timestamp}}.csv")({ timestamp: moment().format(fileTimestampFormat) }));
_csvWriter.pipe(fse.createWriteStream(fileName));
function cleanFiles(opt, fileFormat, retentionNumber) {
    retentionNumber = retentionNumber || 2;
    fileFormat = fileFormat || fileTimestampFormat;
    glob(path.resolve("data/cambodia-postcode-latlng-*.csv"), {}, function (err, files) {
        files = _.sortBy(files, function (file) {
            var fileName = path.basename(file);
            return moment(fileName, fileFormat);
        });
        var toDeleteCount = files.length - retentionNumber;
        if (toDeleteCount > 0) {
            var toDeleteFiles = _.take(files, toDeleteCount);
            _.each(toDeleteFiles, function (file) {
                fse.removeSync(file);
            });
        }
    });
}
var options = {
    provider: 'google',
    // Optional depending on the providers 
    httpAdapter: 'https',
    apiKey: 'AIzaSyCUtJSrsFA6iJExRa5hVJFCIUg07RDpyZg',
    formatter: null // 'gpx', 'string', ... 
};
var geocoder = NodeGeocoder(options);
var geoCodeFn = function (address, cb) {
    geocoder.geocode({ address: address, country: 'Cambodia' }, function (err, res) {
        cb(err, res);
    });
};
function getGeoCode(address, cb) {
    memoize.fn(geoCodeFn).then(function (_geoCodeFn) {
        _geoCodeFn(address, cb).then(function (result) {
            return _geoCodeFn(address, _.noop); // cache hit 
        }).then()
            .catch();
    }).catch();
}
var queue = async.queue(function (data, next) {
    var address = null;
    if (data.type == 'province') {
    }
    else if (data.type == 'district') {
        if (!data.longitude || !data.latitude) {
            address = data.enDistrict + ', ' + data.enProvince;
        }
    }
    else if (data.type == 'commune') {
        if (!data.longitude || !data.latitude) {
            address = data.enCommune + ',' + data.enDistrict + ', ' + data.enProvince;
        }
    }
    if (address) {
        winston.info(address);
        getGeoCode(address, function (err, rs) {
            if (err) {
                winston.error(err);
                _csvWriter.write(data);
                return next();
            }
            var first = _.first(rs);
            winston.info(address, first);
            if (first && first.extra.confidence > 0.4) {
                data.latitude = first.latitude;
                data.longitude = first.longitude;
            }
            _csvWriter.write(data);
            return next();
        });
    }
    else {
        _csvWriter.write(data);
        return next();
    }
}, 1);
csv
    .fromPath(csvFile, { headers: true })
    .on("data", function (data) {
    queue.push(data);
})
    .on("end", function () {
    // queue.drian = function () {
    // _csvWriter.end();
    cleanFiles(null, null, null);
    winston.info('File has been saved to: %s', fileName);
    winston.info('done');
    // }
});
//# sourceMappingURL=updateGeoCodes.1.js.map