'use strict';
require('./libs/bootstrap');
var winston = require('./libs/winston'),
    fse = require('fs-extra'),
    request = require('request'),
    xml2js = require('xml2js');

var parser = new xml2js.Parser();
var xmlFilePath = __dirname + '/config/allagents.xml';

winston.info('Downloading...')
request('http://www.user-agents.org/allagents.xml', function (err, response, body) {
    if (err) {
        winston.error(err);
    } else {
        fse.writeFileSync(xmlFilePath, body);

        parser.parseString(body, function (err, result) {
            var userAgents = result['user-agents']['user-agent'].map(function (x) { return x.String[0] }).filter(function (x) {
                return x.indexOf('Windows') > -1 || x.indexOf('AppleWebKit') > -1;
            })

            fse.writeJsonSync(xmlFilePath.replace('.xml', '.json'), userAgents);

            winston.info('File has been written to: ', xmlFilePath.replace('.xml', '.json'));
            process.exit();
        });
    }
});