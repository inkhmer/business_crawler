var config = require('config'),
	fs = require('fs'),
	fse = require('fs-extra'),
	winston = require('./winston'),
	Url = require('url'),
	_ = require('lodash'),
	request = require('request'),
	path = require('path'),
	mime = require('mime'),
	filesize = require('filesize'),
	mkdirp = require('mkdirp'),
	Agent = require('socks5-http-client/lib/Agent'),
	baseDir = 'D:\\Projects\\NodeJs\\business_crawler\\data\\images';

if (!fs.existsSync(baseDir)) {
	fs.mkdir(baseDir);
}

function getExtFromUri(uri) {
	var regex = /[#\\?]/g; // regex of illegal extension characters
	var extname = path.extname(uri);
	var endOfExt = extname.search(regex);
	if (endOfExt > -1) {
		return extname.substring(0, endOfExt);
	}
	return null;
}


// setup download
// http://stackoverflow.com/questions/12740659/downloading-images-with-node-js
var downloadImageFile = function (fullUrl, opt, callback) {
	if (!_.isFunction(callback)) callback = _.noop;

	var dir = path.join(baseDir, opt.path || '');

	var filePath, file;

	var parsedUrl = Url.parse(fullUrl);

	var filename = path.basename(parsedUrl.pathname);

	var ext = getExtFromUri(fullUrl);
	var contentType = null;

	filePath = path.join(dir, filename);

	var contentType = null;

	if (ext) {
		filename += ext;
		contentType = mime.lookup(ext);
	}

	var splits = filePath.split('\\'); // for window
	splits.splice(-1, 1);

	var fullDir = splits.join('\\'); // for window

	if (!fs.existsSync(fullDir)) {
		mkdirp.sync(fullDir);
	}

	var imgExtensions = ['.png', '.jpg', '.jpeg'];

	if ((filePath.indexOf('.') > -1 && fs.existsSync(filePath)) || fs.existsSync(filePath + '.png') || fs.existsSync(filePath + '.jpg') || fs.existsSync(filePath + '.jpeg')) {
		winston.info('Skipped %s', fullUrl);

		_.each(imgExtensions, function (_ext) {
			if (fs.existsSync(filePath + _ext)) {
				filePath = filePath + _ext;
				filename = filename + _ext;
				return false;

			}
		});

		callback(null, { url: fullUrl, filePath: filePath, fileName: filename });
	} else {
		winston.info('Downloading %s', fullUrl);
		request.head(fullUrl, function (err, res, body) {
			var r = request(fullUrl).pipe(fs.createWriteStream(filePath));
			r.on('close', function () {
				if (filename.indexOf('.') == -1) {
					contentType = res.headers["content-type"];
					var _mime = mime.extension(contentType);

					if (_mime) {
						filename += '.' + _mime;
					}
					var newfilePath = path.join(dir, filename);
					fs.renameSync(filePath, newfilePath);
					filePath = newfilePath;
				}

				if (fs.existsSync(filePath)) {
					winston.info('Saved to: %s', filePath);

					callback(null, { url: fullUrl, filePath: filePath, fileName: filename });
				} else {
					callback(new Error('Cannot find downloaded file'));
				}
			})
		})
	}
}

module.exports = {
	downloadImageFile: downloadImageFile
}