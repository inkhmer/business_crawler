"use strict";
var config = require('config'), appRoot = require('app-root-path').path, path = require('path'), AhoCorasick = require('node-aho-corasick'), utils = require('./utils'), _ = require('lodash'), winston = require('winston'), fse = require('fs-extra');
var defaultMapperPath = path.resolve(appRoot, 'mappers/defaults.json');
var mapperKeys = [];
var MapperBuilder = (function () {
    function MapperBuilder() {
    }
    MapperBuilder.cleanJsonFileSync = function () {
        var mappingDict = fse.readJsonSync(defaultMapperPath);
        mapperKeys = _.keys(mappingDict);
        _.each(mappingDict, function (value, key) {
            var newItems = [], items = value;
            if (_.isString(items[0])) {
                items.sort();
                _.each(items, function (item, idx) {
                    newItems.push({
                        id: utils.nextIntId(),
                        name: item,
                        children: []
                    });
                });
            }
            else {
                _.each(items, function (item, idx) {
                    item.id = item.id || utils.nextIntId(),
                        newItems.push(item);
                });
            }
            mappingDict[key] = _.sortBy(newItems, 'name');
        });
        fse.writeJsonSync(defaultMapperPath, mappingDict, { spaces: 2 });
        winston.info('Saved to %s', defaultMapperPath);
    };
    MapperBuilder.updateListItem = function (items, list) {
        _.each(items, function (item) {
            list[item] = list[item] || [];
            var extras = [item];
            var splits = item.split('-');
            if (splits.length == 2) {
                extras.push(splits.join(''));
                extras.push(splits.join(' '));
            }
            var and = item.replace(/\s&\s/, ' and ');
            var and2 = item.replace(/\sand\s/, ' & ');
            if (and != item) {
                extras.push(and);
            }
            if (and2 != item) {
                extras.push(and2);
            }
            extras = _.uniq(extras);
            list[item] = _.uniq(list[item].concat(extras));
        });
    };
    MapperBuilder.buildSourceMappingDataFileSync = function (fileName) {
        var json = fse.existsSync(fileName) ? fse.readJsonSync(fileName) : {}, mappingDict = fse.readJsonSync(defaultMapperPath);
        var defaultJson = {};
        _.keys(mappingDict).forEach(function (key) {
            defaultJson[key] = {};
        });
        json = _.merge(defaultJson, json);
        _.each(mappingDict, function (value, key) {
            var names = _.map(value, 'name');
            names.sort();
            MapperBuilder.updateListItem(names, json[key]);
        });
        fse.writeJsonSync(fileName, json);
        winston.info('Saved to %s', fileName);
    };
    return MapperBuilder;
}());
MapperBuilder.cleanJsonFileSync();
var _Mapper = (function () {
    function _Mapper(fileName, key) {
        this._obj = {};
        this._ac = new AhoCorasick();
        var self = this, mappingJson = fse.existsSync(fileName) ? fse.readJsonSync(fileName) : {};
        _.forOwn(utils.getImpl(mappingJson, key), function (value, key) {
            _.each(value, function (item) {
                var lowered = item.toLowerCase();
                self._obj[lowered] = key;
                self._ac.add(lowered);
            });
        });
        self._ac.build();
    }
    _Mapper.prototype._get = function (key) {
        var self = this;
        if (_.isArray(key)) {
            var items = [];
            _.each(key, function (item) {
                var found = self._ac.search(item.toLowerCase());
                if (found.length > 0) {
                    var first = found[0];
                    if (self._obj[first]) {
                        items.push(self._obj[first]);
                    }
                }
            });
            return items;
        }
        else if (_.isString(key)) {
            var found = self._ac.search(key.toLowerCase());
            if (found.length == 0)
                return null;
            var first = found[0];
            return self._obj[first];
        }
        return null;
    };
    _Mapper.prototype.get = function () {
        var rs = [], self = this;
        if (arguments.length > 0) {
            _.each(arguments, function (arg) {
                var t = self._get(arg);
                rs = rs.concat(_.isArray(t) ? t : (t ? [t] : []));
            });
        }
        return _.uniq(rs);
    };
    _Mapper.prototype.firstOrDefault = function () {
        var self = this, value = null;
        _.each(arguments, function (key) {
            value = self._get(key);
            if (value)
                return false;
        });
        return value;
    };
    return _Mapper;
}());
var Mapper = (function () {
    function Mapper(name) {
        this._cache = {};
        this._cache[name] = this._cache[name] || {};
        var self = this, fileName = path.join(appRoot, '/mappers/' + name + '.json');
        _.each(mapperKeys, function (key) {
            self[key] = self._cache[name][key] || new _Mapper(fileName, key);
            self._cache[name][key] = self[key];
        });
        MapperBuilder.buildSourceMappingDataFileSync(fileName);
    }
    return Mapper;
}());
module.exports = Mapper;
//# sourceMappingURL=mapper.js.map