/**
 * Created with IntelliJ IDEA.
 * User: Alexander <estliberitas> Makarenko
 * Date: 04.01.13
 * Time: 3:25
 */
'use strict';

var fs = require('fs')
    , net = require('net')
    , q = require('q')
    , path = require('path')
    , util = require('util')
    , async = require('async')
    , cookiePath = path.resolve("D:\\Apps\\Tor Browser\\Browser\\TorBrowser\\Data\\Tor\\control_auth_cookie")
    , winston = require('./winston')
    , cookie = fs.readFileSync(cookiePath).toString('hex');


/**
 * Request tor get new identity, i.e. reset circuit
 *
 * @param {String} host
 *    Hostname of Tor control service
 * @param {String|Number} port
 *    Tor control port
 * @param {String} [auth]
 *    Auth cookie (optional)
 * @param {Function} callback
 *    Callback taking result (err)
 */
function new_identity(host, port, auth, callback) {
    if (typeof auth === 'function') {
        callback = auth;
        auth = null;
    }

    var sock = new net.Socket({ allowHalfOpen: false });
    connect(sock, port, host)
        .then(function () {
            return write(sock, util.format('AUTHENTICATE %s', auth));
        })
        .then(function () {
            return write(sock, 'signal NEWNYM');
        })
        .then(function () {
            sock.destroy();
            callback();
        })
        .fail(function (err) {
            sock.destroy();
            callback(err);
        });
}


/**
 * Write data to socket in promise-style
 *
 * @param {net.Socket} sock
 *    Socket object
 * @param {String} cmd
 *    Command to execute
 * @return {promise}
 *    Promise object
 */
function write(sock, cmd) {
    var deferred = q.defer();
    if (!sock.writable) {
        process.nextTick(function () {
            deferred.reject(new Error('Socket is not writable'));
        });
        return deferred.promise;
    }

    sock.removeAllListeners('error');
    sock.removeAllListeners('data');

    sock.once('data', function (data) {
        var res = data.toString().replace(/[\r\n]/g, '')
            , tokens = res.split(' ')
            , code = parseInt(tokens[0]);

        if (code !== 250) {
            deferred.reject(new Error(res));
        } else {
            deferred.resolve();
        }
    });

    sock.once('err', deferred.reject);
    sock.write(cmd + '\r\n');
    return deferred.promise;
}


/**
 * Connect to Tor control service in promise-style
 *
 * @param {net.Socket} sock
 *    Socket object
 * @param {String|Number} port
 *    Tor control port
 * @param host
 *    Tor control host
 * @return {promise}
 *    Promise object
 */
function connect(sock, port, host) {
    var deferred = q.defer();
    sock.once('connect', deferred.resolve);
    sock.once('error', deferred.reject);
    sock.connect(port, host);
    return deferred.promise;
}

var request = require('request');
var requestOpts = {
    method: 'GET',
    url: 'http://ipv4.icanhazip.com/',
    headers: {
        'User-Agent': 'Mozilla/5.0 (compatible; YandexDirect/3.0; +http://yandex.com/bots)'
    },

    agentClass: require('socks5-http-client/lib/Agent'),
    agentOptions: {
        socksHost: 'localhost', // Defaults to 'localhost'.
        socksPort: 9150 // Defaults to 1080.
    }
};


module.exports = {
    newIdentity: function (cb) {
        cb = cb || function () { };

        winston.info('')
        winston.info('Changing ip....');
        var oldIp = null;
        var hasSameIp = true;

        request(requestOpts, function (err, res) {
            if (err) {
                winston.error(err);
            } else {
                winston.info('Tor old ip: ' + res.body.trim());
                oldIp = res.body.trim()
            }


            async.retry({
                errorFilter: function (err) {
                    return hasSameIp
                }
            }, function(next) {
                 new_identity('127.0.0.1', 9051, cookie, function (err) {
                    winston.info('Changing ip.... has finished...');
                    request(requestOpts, function (err, res) {
                        if (err) {
                            winston.error(err);
                        } else {
                            winston.info('Tor has a new ip: ' + res.body.trim());
                            winston.info('')
                        }
                        hasSameIp = oldIp && oldIp == res.body.trim();
                        next();
                    });
                });
            }, cb);
        });
    }
}