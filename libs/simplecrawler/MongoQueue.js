/*
 * Simplecrawler - queue module
 * https://github.com/cgiffard/node-simplecrawler
 *
 * Copyright (c) 2011-2015, Christopher Giffard
 *
 */

var fs = require("fs");
var _ = require('lodash'),
  cheerio = require('cheerio'),
  QueueItem = require("./QueueItem");


var allowedStatistics = [
  "requestTime",
  "requestLatency",
  "downloadTime",
  "contentLength",
  "actualDataSize"
];

var FetchQueue = function (mongo, crawler) {
  this.crawler = crawler;
  console.log("Initializing mongo queue for " + this.crawler.name);

  this.Item = QueueItem;

  this.updateStatus = function (item, status) {
    // console.log("" + status + " \t " + item.url);
    item.set({
      status: status
    });

    item.save(_.noop);
  };
  var self = this;

  var eventstates = {
    fetchstart: 'spooled',
    //fetchcomplete: 'fetched',
    fetcherror: 'error',
    fetchredirect: 'redirected'
  };

  var setEventHandler = function (event, status) {
    return self.crawler.on(event, function (item) {
      return self.updateStatus(item, status);
    });
  };

  this.crawler.discoverResources = function (buffer, queueItem) {
    var $ = cheerio.load(buffer.toString("utf8"));

    var urls = $("a[href]").map(function () {
      // return 'http://'+ host + $(this).attr('href');
      var href = $(this).attr('href');
      return href.replace(/^\/\//, '');
    }).get();
    console.log(urls);
    return urls;
  };

  _.each(eventstates, function (event, key) {
    setEventHandler(key, event);
  });

  this.crawler.on("fetchcomplete", function (queueItem, responseBuffer, response) {
    queueItem.set({ content: responseBuffer.toString("utf8"), status: 'fetched' })
    queueItem.save(this.wait());

    console.log("downloaded: %s", queueItem.url);
    console.log('');
  });


  var query = {
    status: 'spooled',
    crawler: self.crawler.name
  };

  this.Item.update(query, {
    status: 'queued'
  }, {
      multi: true
    }, function (error) {
      if (error) {
        throw error;
      }
    });
}

module.exports = FetchQueue;

// FetchQueue.prototype = [];
FetchQueue.prototype.add = function (protocol, host, port, path, depth, callback) {

  // For legacy reasons
  if (depth instanceof Function) {
    callback = depth;
    depth = 1;
  }

  depth = depth || 1;
  callback = callback && callback instanceof Function ? callback : function () { };
  var self = this;

  // Ensure all variables conform to reasonable defaults
  protocol = protocol === "https" ? "https" : "http";

  if (isNaN(port) || !port) {
    return callback(new Error("Port must be numeric!"));
  }

  var url = protocol + "://" + host + (port !== 80 ? ":" + port : "") + path;
  var self = this;

  self.exists(protocol, host, port, path,
    function (err, exists) {
      if (err) {
        return callback(err);
      }

      if (!exists) {
        var queueItem = {
          url: url,
          protocol: protocol,
          host: host,
          port: port,
          path: path,
          depth: depth,
          fetched: false,
          status: "queued",
          crawler: self.crawler.name,
          stateData: {}
        };

        // self.push(queueItem);
        // callback(null, queueItem);

        self.Item.findOne({
          protocol: protocol,
          host: host,
          port: port,
          path: path,
          crawler: self.crawler.name
        }, function (error, item) {
          // callback(null, queueItem);
          if (item) {
            item.set(queueItem);
          } else {
            item = new QueueItem(queueItem);
          }

          item.save(callback);
        });

      } else {
        var error = new Error("Resource already exists in queue!");
        error.code = "DUP";

        callback(error);
      }
    });
};

// Check if an item already exists in the queue...
FetchQueue.prototype.exists = function (protocol, host, port, path, callback) {
  callback = _.isFunction(callback) ? callback : _.noop;

  var data;
  data = {
    protocol: protocol,
    host: host,
    port: port,
    path: path,
    crawler: this.crawler.name
  };

  this.Item.count(data, callback);
};

// Get last item in queue...
FetchQueue.prototype.last = function (callback) {
  callback = _.isFunction(callback) ? callback : _.noop;
  return this.Item.findOne({
    crawler: this.crawler.name
  }).sort({
    id: -1
  }).exec(callback);
};

// Get item from queue
FetchQueue.prototype.get = function (id, callback) {
  callback = _.isFunction(callback) ? callback : _.noop;
  console.log('id', id);

  // var item,
  //   self = this;

  // if (!isNaN(id) && self.length > id) {
  //   item = self[id];
  //   callback(null, item);
  //   return item;
  // }
  return this.Item.findById(id, callback);
};

// Get first unfetched item in the queue (and return its index)
FetchQueue.prototype.oldestUnfetchedItem = function (callback) {
  callback = _.isFunction(callback) ? callback : _.noop;
  // var item,
  //   self = this;

  // for (var itemIndex = self.oldestUnfetchedIndex; itemIndex < self.length; itemIndex++) {
  //   if (self[itemIndex].status === "queued") {
  //     self.oldestUnfetchedIndex = itemIndex;
  //     item = self[itemIndex];
  //     console.log('item -----------', item);
  //     callback(null, item);
  //     return item;
  //   }
  // }

  // callback(new Error("No unfetched items remain."));
  // return;

  var query = {
    crawler: this.crawler.name,
    status: 'queued'
  };

  this.Item.findOne(query, callback);
  // return this.Item.findOneAndUpdate(query, {
  //   status: 'spooled'
  // }, callback);
};

// Gets the maximum total request time, request latency, or download time
FetchQueue.prototype.max = function (statisticName, callback) {
  callback = _.isFunction(callback) ? callback : _.noop;
  // var maxStatisticValue = 0,
  //   self = this;

  // if (allowedStatistics.join().indexOf(statisticName) === -1) {
  //   // Not a recognised statistic!
  //   return callback(new Error("Invalid statistic."));
  // }

  // self.forEach(function (item) {
  //   if (item.fetched && item.stateData[statisticName] !== null && item.stateData[statisticName] > maxStatisticValue) {
  //     maxStatisticValue = item.stateData[statisticName];
  //   }
  // });

  // callback(null, maxStatisticValue);
  // return maxStatisticValue;
  return process.nextTick(function () {
    return callback(null, 0);
  });
};

// Gets the minimum total request time, request latency, or download time
FetchQueue.prototype.min = function (statisticName, callback) {
  callback = _.isFunction(callback) ? callback : _.noop;
  return process.nextTick(function () {
    return callback(null, 0);
  });

  // var minimum,
  //   minStatisticValue = Infinity,
  //   self = this;

  // if (allowedStatistics.join().indexOf(statisticName) === -1) {
  //   // Not a recognised statistic!
  //   return callback(new Error("Invalid statistic."));
  // }

  // self.forEach(function (item) {
  //   if (item.fetched && item.stateData[statisticName] !== null && item.stateData[statisticName] < minStatisticValue) {
  //     minStatisticValue = item.stateData[statisticName];
  //   }
  // });

  // minimum = minStatisticValue === Infinity ? 0 : minStatisticValue;
  // callback(null, minimum);
  // return minimum;
};

// Gets the minimum total request time, request latency, or download time
FetchQueue.prototype.avg = function (statisticName, callback) {
  callback = _.isFunction(callback) ? callback : _.noop;

  return process.nextTick(function () {
    return callback(null, 0);
  });

  // var average,
  //   NumberSum = 0,
  //   NumberCount = 0,
  //   self = this;

  // if (allowedStatistics.join().indexOf(statisticName) === -1) {
  //   // Not a recognised statistic!
  //   return callback(new Error("Invalid statistic."));
  // }

  // self.forEach(function (item) {
  //   if (item.fetched && item.stateData[statisticName] !== null && !isNaN(item.stateData[statisticName])) {
  //     NumberSum += item.stateData[statisticName];
  //     NumberCount++;
  //   }
  // });
  // average = NumberSum / NumberCount;
  // callback(null, average);
  // return average;
};

// Gets the number of requests which have been completed.
FetchQueue.prototype.complete = function (callback) {
  callback = _.isFunction(callback) ? callback : _.noop;

  this.Item.count({
    crawler: this.crawler.name,
    fetched: true
  }, callback)
};

// Gets the number of queue items with the given status
FetchQueue.prototype.countWithStatus = function (status, callback) {
  callback = _.isFunction(callback) ? callback : _.noop;

  return this.Item.count({
    status: status,
    crawler: this.crawler.name
  });
};

// Gets the number of queue items with the given status
FetchQueue.prototype.getWithStatus = function (status, callback) {
  callback = _.isFunction(callback) ? callback : _.noop;

  return this.Item.find({
    status: status,
    crawler: this.crawler.name
  }, callback);
};

// Gets the number of requests which have failed for some reason
FetchQueue.prototype.errors = function (callback) {
  callback = _.isFunction(callback) ? callback : _.noop;

  return this.Item.count({
    status: 'error',
    crawler: this.crawler.name
  }, callback);
};

// Gets the number of items in the queue
FetchQueue.prototype.getLength = function (callback) {
  callback = _.isFunction(callback) ? callback : _.noop;

  this.Item.count({
    crawler: this.crawler.name
  }, function (err, count) {
    callback(err, count);
  });
};

// Writes the queue to disk
FetchQueue.prototype.freeze = function (filename, callback) {
  callback = _.isFunction(callback) ? callback : _.noop;

  var self = this;

  // Re-queue in-progress items before freezing...
  self.forEach(function (item) {
    if (item.fetched !== true) {
      item.status = "queued";
    }
  });

  fs.writeFile(filename, JSON.stringify(self), function (err) {
    callback(err, self);
  });
};

// Reads the queue from disk
FetchQueue.prototype.defrost = function (filename, callback) {
  callback = _.isFunction(callback) ? callback : _.noop;

  var self = this,
    defrostedQueue = [];

  fs.readFile(filename, function (err, fileData) {
    if (err) {
      return callback(err);
    }

    if (!fileData.toString("utf8").length) {
      return callback(new Error("Failed to defrost queue from zero-length JSON."));
    }

    try {
      defrostedQueue = JSON.parse(fileData.toString("utf8"));
    } catch (error) {
      return callback(error);
    }

    self.oldestUnfetchedIndex = Infinity;
    self.scanIndex = {};

    for (var index in defrostedQueue) {
      if (defrostedQueue.hasOwnProperty(index) && !isNaN(index)) {
        var queueItem = defrostedQueue[index];
        self.push(queueItem);

        if (queueItem.status !== "downloaded") {
          self.oldestUnfetchedIndex = Math.min(
            self.oldestUnfetchedIndex, index);
        }

        self.scanIndex[queueItem.url] = true;
      }
    }

    if (self.oldestUnfetchedIndex === Infinity) {
      self.oldestUnfetchedIndex = 0;
    }

    callback(null, self);
  });
};