var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var QueueItemSchema = new Schema({
  protocol: {
    type: String,
    required: true,
    index: true
  },
  host: {
    type: String,
    required: true,
    index: true
  },
  port: {
    type: Number,
    index: true
  },
  path: {
    type: String,
    required: true,
    index: true
  },
  status: {
    type: String,
    "default": 'queued',
    index: true,
    "enum": ['queued', 'spooled', 'fetched', 'redirected', 'error', 'downloaded']
  },
  fetched: {
    type: Boolean,
    "default": false
  },
  stateData: {
    type: Object,
    "default": {}
  },
  crawler: {
    type: String,
    index: true,
    "default": 'default'
  },
  depth: Number,
  referrer: String,
  
  content: {
    type: String
  }
  // content: { 
  //   data: Buffer, 
  //   contentType: String,
  //   get: function (data) {
  //       var buf = new Buffer(text, 'utf-8');   // Choose encoding for the string.
  //       zlib.gzip(buf, function (_, result) {  // The callback will give you the 
  //         res.end(result);                     // result, so just send it.
  //       });
  //   },
  //   set: function (data) {
  //     return JSON.stringify(data);
  //   }
  //     }
});

QueueItemSchema.index({
  crawler: 1,
  status: 1
});

QueueItemSchema.virtual('url').get(function() {
  return this.protocol + '://' + this.host + (this.port ? ':' + this.port : void 0) + this.path;
});

module.exports  = mongoose.model('QueueItem', QueueItemSchema);

