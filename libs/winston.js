var config = require('config'),
    // exceptionless = require('../node_modules/exceptionless/dist/exceptionless.node'),
    // client = exceptionless.ExceptionlessClient.default,
    _ = require('lodash'),
	fse = require('fs-extra'),
    util = require('util'),
	path = require('path'),
	moment = require('moment'),
	winston = require('winston');

var date = moment();
var logFolder = path.resolve('logs/main/', date.format('YYYY_MM_DD'));
fse.ensureDirSync(logFolder);

var logger = new (winston.Logger)({
	exitOnError: false,
	transports: [
		new (winston.transports.Console)({
			colorize: true,
			prettyPrint: true
		}),
		new (winston.transports.File)({
			name: 'info-file',
			json: false,
			filename: logFolder + date.format('/hh_mm_ss') + '-info.log',
			level: 'verbose',
		}),
		new (winston.transports.File)({
			name: 'error-file',
			json: false,
			filename: logFolder + date.format('/hh_mm_ss') + '-error.log',
			level: 'error'
		})
	]
});

module.exports = logger;



// // Temp solution;
// client.config = _.extend(client.config, config.get('exceptionlessConfig'));

// // Remove environmentInfoPlugin
// _.remove(client.config._plugins, function (x) { return x.name === 'EnvironmentInfoPlugin' });


// // Create exceptionless logger
// var ExceptionlessLogger = winston.transports.ExceptionlessLogger = function (options) {
//     this.name = 'exceptionlessLogger';
//     this.level = options.level || 'info';
// };

// util.inherits(ExceptionlessLogger, winston.Transport);

// // Create log method
// ExceptionlessLogger.prototype.log = function (level, msg, meta, callback) {
//     var event;

//     if (level === 'error') {
//         if (meta.type == 'http') {
//             event = client.createNotFound(meta.url);
//         } else {
//             event = client.createException(meta._error).markAsCritical();
//             delete meta._error;
//         }
//     } else {
//         event = client.createLog('client-checker', msg, level);
//     }
//     if (event) {
//         _.forOwn(meta, function (v, k) { if (k !== 'tags') event.setProperty(k, v); });
//         event.addTags.apply(event, meta.tags);
//         event.submit(callback);
//     }
// };

// var getLogger = function (tags) {
//     tags = _.isArray(tags) ? tags : (tags ? [tags] : []);

//     var transports = [
//         new (winston.transports.ExceptionlessLogger)({
//             level: 'info'
//         })
//     ];

//     // in Dev
//     if (process.env.NODE_ENV !== 'production') {
//         transports.push(new (winston.transports.Console)({
//             colorize: true,
//             prettyPrint: function (meta) {
//                 var output = '';

//                 if (meta !== null && meta !== undefined) {
//                     if (meta && meta instanceof Error && meta.stack) {
//                         meta = meta.stack;
//                     }
//                     delete meta.tags;
//                     if (typeof meta !== 'object') {
//                         output += ' ' + meta;
//                     }
//                     else if (Object.keys(meta).length > 0) {
//                         output += '\n' + util.inspect(meta, false, null, true);
//                     }
//                 }

//                 return output;
//             },
//             level: 'debug'
//         }));
//     }

//     var logger = new (winston.Logger)({
//         exitOnError: false,
//         rewriters: [
//             function (level, msg, meta, l) {
//                 if (_.isError(meta)) {
//                     meta._error = _.extend(meta);
//                 } else if (_.isError(msg)) {
//                     meta._error = _.extend(msg);
//                 }

//                 meta.tags = _.unique(['NodeJs'].concat(tags).concat(meta.tags || []));
//                 return meta;
//             }
//         ],
//         transports: transports
//     });

//     return logger;
// };

// module.exports = {
//     'default': getLogger(),
//     getLogger: getLogger
// }

























// var logger = new (winston.Logger)({
// 	exitOnError: false,
//     transports: [
// 		new (winston.transports.Console)({
// 			colorize: true,
// 			prettyPrint: true
// 		}),
// 		// new (winston.transports.File)({
// 		// 	name: 'info-file',
// 		// 	json: false,
// 		// 	filename: './logs/filelog-info.log',
// 		// 	level: 'info'
// 		// }),
// 		// new (winston.transports.File)({
// 		// 	name: 'error-file',
// 		// 	json: false,
// 		// 	filename: './logs/filelog-error.log',
// 		// 	level: 'error'
// 		// })
//     ]
// });

// module.exports = logger;