var _ = require('lodash');

function findTextAndReturnRemainder(target, variable) {
    var chopFront = target.substring(target.search(variable) + variable.length, target.length);
    var result = chopFront.substring(0, chopFront.search(";"));
    return result;
}

function findVarValue(target, variable) {
    var str = findTextAndReturnRemainder(target, variable);

    if (!str) return null;
    var splits = str.split('=');
    if (splits.length > 2) {
        return splits[1].replace(/var.*/,'').trim().trim();
    }
    return _.last(splits).trim();
}


module.exports = {
    findTextAndReturnRemainder: findTextAndReturnRemainder,
    findVarValue: findVarValue
};