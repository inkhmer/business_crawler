// var cacheManager = require('cache-manager'),
//     fsStore = require('cache-manager-fs-binary'),
//     defaultTtl = 24 * 60 * 60,
//     async = require('async'),
//     path = require('path'),
//     _ = require('lodash'),
//     fse = require('fs-extra'),
//     _isReady = false;

// module.exports = function (dirName, opts) {
//     var diskCacheDir = path.resolve('cache/diskcache', dirName);

//     fse.ensureDirSync(diskCacheDir);
//     diskCache = cacheManager.caching({
//         store: fsStore,
//         options: _.extend({
//             ttl: defaultTtl,
//             maxsize: 1000 * 1000 * 1000,
//             path: diskCacheDir,
//             preventfill: false,
//             fillcallback: function () {
//                 _isReady = true;
//             },
//             zip: true
//         }, opts)
//     });

//     function isReady(cb) { setTimeout(cb, 50); }

//     diskCache.ensureReady = function ensureReady(cb) {
//         async.until(function () { return _isReady }, isReady, cb);
//     }

//     return diskCache;
// };