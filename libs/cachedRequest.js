var request = require('request'),
    cachedRequest = require('cached-request')(request),
    cacheDirectory = "cache/request-cache";

cachedRequest.setCacheDirectory(cacheDirectory);

module.exports = cachedRequest;