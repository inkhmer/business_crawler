var fs = require('fs'),
	_ = require('lodash'),
	path = require('path'),
	validator = require('validator'),
	URI = require('urijs'),
	getUrls = require('get-urls'),
	request = require('request');

var getImpl = function (object, property) {
	var t = this,
		elems = Array.isArray(property) ? property : property.split('.'),
		name = elems[0],
		value = object[name];
	if (elems.length <= 1) {
		return value;
	}
	if (typeof value !== 'object') {
		return undefined;
	}
	return getImpl(value, elems.slice(1));
};


var basePath = "images/";

if (!fs.existsSync(basePath)) {
	fs.mkdir(basePath);
}

// setup download
// http://stackoverflow.com/questions/12740659/downloading-images-with-node-js
var downloadFile = function (uri, filename, callback) {
	var file = filename.split('/')[filename.split('/').length - 1];

	if (fs.existsSync(file)) callback();
	else {
		request.head(uri, function (err, res, body) {
			var r = request(uri).pipe(fs.createWriteStream(basePath + file));
			r.on('close', callback);
		});
	}
};


module.exports = {
	removeExtraSpace: function (str) {
		if (!str) return null;
		return str.replace(/\s+/g, ' ').replace(/\r?\n|\r/g, '').trim();
	},
	toTitleCase: function (str) {
		if (!str) return null;
		str = str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });

		str = str.replace(/bbq/i, 'BBQ');

		return str;
	},
	getImpl: getImpl,
	downloadFile: downloadFile,
	nextIntId: function () {
		var fileName = __dirname + '/../data/id_seed.txt';
		var n;
		try {
			var nn = fs.readFileSync(fileName);
			var n = _.parseInt(nn);
		} catch (ex) {

		}
		if (_.isNaN(n) || !n) { n = 100; }
		n++;

		fs.writeFileSync(fileName, n);
		return n;
	},
	nextListingId: function () {
		var fileName = __dirname + '/../data/listing_seed_id.txt';
		var n;
		try {
			var nn = fs.readFileSync(fileName);
			var n = _.parseInt(nn);
		} catch (ex) {

		}
		if (_.isNaN(n) || !n) { n = 1; }
		n++;

		fs.writeFileSync(fileName, n);
		return n;
	},
	getExtFromUri: function (uri) {
		var regex = /[#\\?]/g; // regex of illegal extension characters
		var extname = path.extname(uri);
		var endOfExt = extname.search(regex);
		if (endOfExt > -1) {
			return extname.substring(0, endOfExt);
		}
		return null;
	},
	findEmails: function (text) {
		var emails = text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi) || [];
		return _.unique(_.filter(emails.map(function (x) { return x.toLowerCase(); })), function (x) {
			return validator.isEmail(x);
		});
	},
	findUrls: function (text) {
		if (!text) return [];

		var urlArray = [];
		var matchArray,
			source = text.replace(/http:\/\/www\./, 'www.').replace(/www\./, 'http://www.');

		try {
			urlArray = getUrls(source);
		} catch (ex) {
		}

		if (urlArray.length == 0) {

			// Regular expression to find FTP, HTTP(S) and email URLs.
			var regexToken = /^(http:\/\/)+(www\.)?\w*\.(com|net|org|biz|com\.kh)$/gm;

			// Iterate through any URLs in the text.
			while ((matchArray = regexToken.exec(source)) !== null) {
				var token = matchArray[0];
				urlArray.push(token);
			}
		}
		for (var i = 0; i < urlArray.length; i++) {
			urlArray[i] = this.hostnameFromUrl(urlArray[i]);
		}

		return  _.unique(_.filter(urlArray, function (x) {
			return validator.isURL(x);
		}));
	},
	getValueFromBracket: function (text) {
		var regExp = /\(([^)]+)\)/;
		var matches = regExp.exec(text);
		if (matches.length > 0) {
			return parseInt(matches[1]);
		}
		return null;
	},
	removeSpaceBetween: function (text, d) {
		return text.replace(/\(.*?\)/g, function (text) {
			return text.replace(/\s/g, '');
		});

		// return text.replace(/\s(?![^)]*\()/g, '');
	},
	prepareText: function (text) {
		text = text.replace(/(\s?)((\(|@))/g, '$1 $2');
		text = this.removeExtraSpace(text);
		return text;
	},
	urlFromEmail: function (email) {
		if (!email) return null;
		var emailProviders = [
		/* Default domains included */
			"aol.com", "att.net", "comcast.net", "facebook.com", "gmail.com", "gmx.com", "googlemail.com",
			"google.com", "hotmail.com", "hotmail.co.uk", "mac.com", "me.com", "mail.com", "msn.com",
			"live.com", "sbcglobal.net", "verizon.net", "yahoo.com", "yahoo.co.uk",

		/* Other global domains */
			"email.com", "games.com" /* AOL */, "gmx.net", "hush.com", "hushmail.com", "inbox.com",
			"lavabit.com", "love.com" /* AOL */, "pobox.com", "rocketmail.com" /* Yahoo */,
			"safe-mail.net", "wow.com" /* AOL */, "ygm.com" /* AOL */, "ymail.com" /* Yahoo */, "zoho.com", "fastmail.fm",

		/* United States ISP domains */
			"bellsouth.net", "charter.net", "cox.net", "earthlink.net", "juno.com",

		/* British ISP domains */
			"btinternet.com", "virginmedia.com", "blueyonder.co.uk", "freeserve.co.uk", "live.co.uk",
			"ntlworld.com", "o2.co.uk", "orange.net", "sky.com", "talktalk.co.uk", "tiscali.co.uk",
			"virgin.net", "wanadoo.co.uk", "bt.com",

		/* Domains used in Asia */
			"sina.com", "qq.com", "naver.com", "hanmail.net", "daum.net", "nate.com", "yahoo.co.jp", "yahoo.co.kr", "yahoo.co.id", "yahoo.co.in", "yahoo.com.sg", "yahoo.com.ph",

		/* French ISP domains */
			"hotmail.fr", "live.fr", "laposte.net", "yahoo.fr", "wanadoo.fr", "orange.fr", "gmx.fr", "sfr.fr", "neuf.fr", "free.fr",

		/* German ISP domains */
			"gmx.de", "hotmail.de", "live.de", "online.de", "t-online.de" /* T-Mobile */, "web.de", "yahoo.de",

		/* Russian ISP domains */
			"mail.ru", "rambler.ru", "yandex.ru", "ya.ru", "list.ru",

		/* Belgian ISP domains */
			"hotmail.be", "live.be", "skynet.be", "voo.be", "tvcablenet.be", "telenet.be",

		/* Argentinian ISP domains */
			"hotmail.com.ar", "live.com.ar", "yahoo.com.ar", "fibertel.com.ar", "speedy.com.ar", "arnet.com.ar",

		/* Domains used in Mexico */
			"hotmail.com", "gmail.com", "yahoo.com.mx", "live.com.mx", "yahoo.com", "hotmail.es", "live.com", "hotmail.com.mx", "prodigy.net.mx", "msn.com"
		];

		if (_.any(emailProviders, function (x) { return email.indexOf(x) > -1; })) return null;
		return this.hostnameFromUrl('http://' + _.last(email.split('@'))) || null;
	},
	hostnameFromUrl: function (url) {
		try {
			var uri = URI(url);
			return uri.scheme() + "://www." + uri.hostname();
		} catch (ex) {
		}
		return null;
	}
}