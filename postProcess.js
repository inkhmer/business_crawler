'use strict';
require('./libs/bootstrap');
var winston = require('./libs/winston'), async = require('async'), path = require('path'), csvWriter = require('csv-write-stream'), fse = require('fs-extra'), moment = require('moment'), csv = require("fast-csv"), _ = require('lodash'), trycatch = require('trycatch'), scrape = require('html-metadata'), glob = require("glob"), NodeGeocoder = require('node-geocoder'), validator = require('validator'), cachePath = path.join(__dirname, 'cache', 'cache-memoize'), PNF = require('google-libphonenumber').PhoneNumberFormat, DefaultProvider = require('./providers/defaultProvider'), phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance(), emailExistence = require('email-existence'), memoize = require('memoize-fs')({ cachePath: cachePath });
var fileTimestampFormat = "YYYY-MM-DD_hhmmss";
var csvFile = path.resolve('./data/v0.1/YellowPage-Business-Cambodia.csv');
function cleanFiles(opt, fileFormat, retentionNumber) {
    retentionNumber = retentionNumber || 1;
    fileFormat = fileFormat || fileTimestampFormat;
    glob(path.resolve("data/v0.1/YellowPage-Business-Cambodia-*.csv"), {}, function (err, files) {
        files = _.sortBy(files, function (file) {
            var fileName = path.basename(file);
            return moment(fileName, fileFormat);
        });
        var toDeleteCount = files.length - retentionNumber;
        if (toDeleteCount > 0) {
            var toDeleteFiles = _.take(files, toDeleteCount);
            _.each(toDeleteFiles, function (file) {
                fse.removeSync(file);
            });
        }
    });
}
var options = {
    provider: 'google',
    // Optional depending on the providers 
    httpAdapter: 'https',
    apiKey: 'AIzaSyCUtJSrsFA6iJExRa5hVJFCIUg07RDpyZg',
    formatter: null // 'gpx', 'string', ... 
};
var geocoder = NodeGeocoder(options);
var geoCodeFn = function (address, cb) {
    geocoder.geocode({ address: address, country: 'Cambodia' }, function (err, res) {
        cb(err, res);
    });
};
function getGeoCode(address, cb) {
    memoize.fn(geoCodeFn).then(function (_geoCodeFn) {
        _geoCodeFn(address, cb).then(function (result) {
            return _geoCodeFn(address, _.noop); // cache hit 
        }).then()
            .catch();
    }).catch();
}
var logoDict = {};
var postcodeDict = {};
var categoryDict = {};
var _csvWriter = null;
var fileName = null;
var queue = async.queue(function (data, next) {
    if (data.isLast) {
        cleanFiles(null, null, null);
        winston.info('File has been saved to: %s', fileName);
        winston.info('done');
        process.exit();
    }
    if (!_csvWriter) {
        _csvWriter = csvWriter({ headers: _.keys(data).concat(['logoFilePath', 'category', 'subCategory', 'description']) });
        fileName = path.resolve(_.template("data/v0.1/YellowPage-Business-Cambodia-{{timestamp}}.csv")({ timestamp: moment().format(fileTimestampFormat) }));
        _csvWriter.pipe(fse.createWriteStream(fileName));
    }
    // update logo
    data.logoFilePath = logoDict[data.logo];
    var postcode = postcodeDict[data.addressPostalCode];
    if (postcode) {
        data.addressCommune = postcode.enCommune;
        data.addressDistrict = postcode.enDistrict;
        if (!data.addressProvince) {
            data.addressProvince = postcode.enProvince;
        }
    }
    else {
        data.addressCommune = '';
        data.addressDistrict = '';
    }
    if (data.emails.indexOf('.directoryup.com') > -1) {
        data.emails = '';
    }
    // data = updateCategories(data);
    // data = updateTelephone(data);
    async.waterfall([
        function (cb) {
            cb(null, data);
        },
        // updateDescription,
        verifyEmail
    ], function (err, result) {
        _csvWriter.write(result);
        return next();
    });
}, 1);
function updateCategories(data) {
    var subCategory = null;
    var subCategories = [];
    var tags = data.tags.split(',').map(function (x) { return x.trim(); });
    _.each(tags, function (tag) {
        if (categoryDict[tag.toLowerCase()]) {
            subCategories.push(categoryDict[tag.toLowerCase()]);
        }
        if (!subCategory) {
            subCategory = categoryDict[tag.toLowerCase()];
        }
    });
    if (subCategory) {
        // if (subCategories.length > 1) {
        // winston.info(subCategories)
        // }
        // winston.info('Has Data', subCategory)
        data.category = subCategory.enCategory;
        data.subCategory = subCategory.enSubCategory;
        var keywords = subCategory.enKeywords.split(',').map(function (x) { return x.trim(); });
        tags = tags.concat(keywords);
        tags = _.uniqBy(tags, function (x) { return x.toLowerCase(); });
        data.tags = tags.join(',');
    }
    return data;
}
function updateTelephone(data) {
    var telephones = (data.telephone || '').split('/').map(function (x) { return x.trim(); });
    ;
    telephones = _.map(telephones, function (telephone) {
        if (!telephone) {
            return null;
        }
        telephone = telephone.replace(/[^\d.-\s+]/g, '');
        trycatch(function () {
            var phoneNumber = phoneUtil.parse(telephone, 'KH');
            telephone = phoneUtil.format(phoneNumber, PNF.NATIONAL);
        }, function (err) {
            winston.error('phone: [', telephone, ']', err);
            telephone = null;
        });
        return telephone;
    });
    data.telephone = _.compact(telephones).join('/');
    return data;
}
var defaultProvider = new DefaultProvider();
function updateDescription(data, cb) {
    if (!data.website) {
        return cb(null, data);
    }
    else {
        defaultProvider.cache("description:" + data.website, function (cacheCb) {
            winston.info('Fetching meata description', data.website);
            trycatch(function () {
                scrape(data.website, cacheCb);
            }, function (ex) {
                cacheCb(ex);
            });
        }, function (err, metadata) {
            if (err) {
                if (err.message) {
                    winston.error(data.website, err.message);
                }
                else {
                    winston.error(data.website, err.message);
                }
                data.website = null;
                return cb(null, data);
            }
            else {
                if (metadata && metadata.general && metadata.general.description) {
                    data.description = metadata.general.description;
                    winston.info();
                    winston.info(data.website, 'Has description');
                    winston.info();
                }
                else {
                    winston.warn(data.website, 'Has no description');
                }
                cb(null, data);
            }
        });
    }
}
var invalidEmailCsvWriter = csvWriter({ headers: ['email', 'valid'] });
invalidEmailCsvWriter.pipe(fse.createWriteStream(path.resolve(_.template("data/v0.1/invalid-emails-{{timestamp}}.csv")({ timestamp: moment().format(fileTimestampFormat) }))));
// var emailCsvWriter = csvWriter({ headers: ['email'] });
// emailCsvWriter.pipe(fse.createWriteStream(path.resolve(_.template("data/v0.1/emails-{{timestamp}}.csv")({ timestamp: moment().format(fileTimestampFormat) }))))
function verifyEmail(data, cb) {
    data.emails = _.compact((data.emails || '').split(',')).join(',');
    var emails = _.compact(data.emails.split(',').map(function (x) { return x.trim(); }));
    emails = _.reject(emails, function (email) {
        var isValid = validator.isEmail(email);
        if (!isValid) {
            winston.warn('Invalid email: %s', email);
        }
        else {
            // emailCsvWriter.write({
            //     email: email
            // })
        }
        return !isValid;
    });
    var validEmails = [];
    cb(null, data);
    // async.eachLimit(emails, 5, function (email, nextEmail) {
    //     defaultProvider.cache("check-email:" + email, function (cacheCb) {
    //         winston.info('Verifying email', email)
    //         emailExistence.check(email, cacheCb);
    //     }, function (err, isEmailValid) {
    //         if (err) {
    //              invalidEmailCsvWriter.write({
    //                 email: email,
    //                 valid: err
    //             })
    //             winston.error(email, err)
    //         } else {
    //             if (isEmailValid) {
    //                 validEmails.push(email);
    //                 winston.info(email, 'Valid');
    //             } else {
    //                 invalidEmailCsvWriter.write({
    //                 email: email,
    //                 valid: isEmailValid
    //             })
    //                 winston.warn(email, 'Invalid');
    //             }
    //         }
    //         nextEmail();
    //     });
    // }, function () {
    //     data.emails = validEmails;
    //     cb(null, data);
    // })
}
var onFinishedFn = _.after(3, onFinished);
csv.fromPath('./data/logos-2017-06-04_120344.csv', { headers: true })
    .on('data', function (data) {
    logoDict[data.url] = data.filePath;
})
    .on('end', onFinishedFn);
csv.fromPath('./data/cambodia-postcode-latlng-2017-06-17_100654.csv', { headers: true })
    .on('data', function (data) {
    if (data.type == 'commune') {
        postcodeDict[data.postcode] = data;
    }
})
    .on('end', onFinishedFn);
csv.fromPath('./data/categories-truelocal-2017-06-18_095434.csv', { headers: true })
    .on('data', function (data) {
    if (data.type == 'subCategory') {
        var keywords = data.enKeywords.split(',').map(function (x) { return x.trim(); });
        _.each(keywords, function (keyword) {
            keyword = keyword.toLowerCase();
            if (categoryDict[keyword]) {
                var x = categoryDict[keyword];
                // winston.warn('Exists category:', x.enCategory, 'subCategory:', x.enSubCategory, ' keyword:', keyword)
                // winston.warn('Category:', data.enCategory, 'subCategory:', data.enSubCategory, ' keyword:', keyword)
                // winston.info();
            }
            categoryDict[keyword] = data;
        });
    }
})
    .on('end', onFinishedFn);
cleanFiles(null, null, null);
function onFinished() {
    csv
        .fromPath(csvFile, { headers: true })
        .on("data", function (data) {
        queue.push(data);
    })
        .on("end", function () {
        queue.push({ isLast: true });
    });
}
//# sourceMappingURL=postProcess.js.map