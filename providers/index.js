require('fs').readdirSync(__dirname + '/').forEach(function (file) {
  if (file.match(/\.js$/) !== null && file !== 'index.js' && file !== 'baseProvider.js') {
    var name = file.replace('.js', '');
    var fs = require('./' + file);

    if (!fs.disabled) {
      delete fs.disabled;
      exports[name] = fs;
    }
  }
});