var BaseProvider = require('./baseProvider'),
  async = require('async'),
  jsutil = require('../libs/jsutil'),
  trycatch = require('trycatch'),
  cheerio = require('cheerio'),
  numeral = require('numeral'),
  urljoin = require('url-join'),
  escapeHtml = require('escape-html'),
  // csvWriter = require('csv-write-stream'),
  parse = require('url-parse'),
  _ = require('lodash');

function CambodiaPostcode(csvWriter) {
  this._csvWriter = {};
  this._csvWriter.write = _.noop;
  this.csvWriter = csvWriter;
}

function toTitleCase(str) {
  return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); }).replace(/\s+/g, " ");
}

CambodiaPostcode.prototype = new BaseProvider('CambodiaPostcode', 'http://www.cambodiapostcode.com');

CambodiaPostcode.prototype.getAllEnProvinces = function (cb) {
  var self = this;
  self.get('http://www.cambodiapostcode.com/features.html', function (err, body) {
    if (err) { return cb(err); }
    var $ = cheerio.load(body);

    var provinces = [];

    $('table tr td a').each(function () {
      var a = $(this);
      provinces.push({
        id: $(a.parent()[0].childNodes[0]).text().trim(),
        url: urljoin(self.baseUrl, a.attr('href')).trim(),
        name: toTitleCase(a.text().trim().replace(/\r?\n|\r/g, '').trim())
      })
    });

    _.each(provinces, function (x) {
      var csv = {
        id: x.id,
        url: x.url,
        type: 'province',
        enProvince: x.name
      };

      self._csvWriter.write(csv);
    })

    cb(null, provinces);
  });
}

CambodiaPostcode.prototype.getAllEnDistricts = function (cb) {
  var self = this;

  self.getAllEnProvinces(function (err, provinces) {
    if (err) return cb(err);

    var allDistricts = [];

    async.eachLimit(provinces, 5, function (province, next) {
      self.logger.info(province.url)
      self.get(province.url, function (err, body) {
        if (err) {
          self.logger.error(err)
          return next();
        }

        var $ = cheerio.load(body);

        var districts = [];

        $('table tr td a').each(function () {
          var a = $(this);
          districts.push({
            name: toTitleCase(a.text().trim().replace(/\r?\n|\r/g, '').trim()),
            url: urljoin(self.baseUrl, a.attr('href')).trim(),
            province: province.name
          });
        })

        _.each(districts, function (x) {
          var csv = {
            id: x.id,
            url: x.url,
            type: 'district',
            enProvince: province.name,
            enDistrict: x.name
          };

          self._csvWriter.write(csv);
        })

        allDistricts = allDistricts.concat(districts);

        next();
      })
    }, function () {
      cb(null, allDistricts);
    });
  })
}

CambodiaPostcode.prototype.getAllEnCommunes = function (cb) {
  var self = this;

  self.getAllEnDistricts(function (err, districts) {
    if (err) return cb(err);

    var allCommunes = [];

    async.eachLimit(districts, 5, function (district, next) {
      self.logger.info(district.url)
      self.get(district.url, function (err, body) {
        if (err) {
          self.logger.error(err)
          return next();
        }

        var $ = cheerio.load(body);

        var communes = [];

        $('table tr').each(function (i) {
          if (i > 0) {
            var tds = $(this).find('td');

            communes.push({
              province: district.province,
              district: district.name,
              commune: toTitleCase(tds.first().text().trim().replace(/\r?\n|\r/g, '').trim()),
              postcode: tds.last().text().trim()
            });
          }
        });

        _.each(communes, function (x) {
          var csv = {
            id: x.id,
            url: x.url,
            type: 'commune',
            enProvince: x.province,
            enDistrict: x.district,
            enCommune: x.commune,
            postcode: x.postcode
          };

          self._csvWriter.write(csv);
        })

        allCommunes = allCommunes.concat(communes);
        next();
      })
    }, function () {
      cb(null, allCommunes)
    });
  })
}

CambodiaPostcode.prototype.getAllKhProvinces = function (cb) {
  var self = this;
  self.get('http://www.cambodiapostcode.com/kh/features.html', function (err, body) {
    if (err) { return cb(err); }
    var $ = cheerio.load(body);

    var provinces = [];

    $('table tr td a').each(function () {
      var a = $(this);
      provinces.push({
        id: $(a.parent()[0].childNodes[0]).text().trim(),
        url: urljoin(self.baseUrl, 'kh', a.attr('href')).trim(),
        name: toTitleCase(a.text().trim().replace(/\r?\n|\r/g, '').trim())
      })
    });

    _.each(provinces, function (x) {
      var csv = {
        id: x.id,
        url: x.url,
        type: 'province',
        khProvince: x.name
      };

      self._csvWriter.write(csv);
    })

    cb(null, provinces);
  });
}

CambodiaPostcode.prototype.getAllKhDistricts = function (cb) {
  var self = this;

  self.getAllKhProvinces(function (err, provinces) {
    if (err) return cb(err);

    var allDistricts = [];

    async.eachLimit(provinces, 5, function (province, next) {
      self.logger.info(province.url)
      self.get(province.url, function (err, body) {
        if (err) {
          self.logger.error(err)
          return next();
        }

        var $ = cheerio.load(body);

        var districts = [];

        $('table tr td a').each(function () {
          var a = $(this);
          districts.push({
            name: toTitleCase(a.text().trim().replace(/\r?\n|\r/g, '').trim()),
            url: urljoin(self.baseUrl, 'kh', a.attr('href')).trim(),
            province: province.name
          });
        })

        _.each(districts, function (x) {
          var csv = {
            id: x.id,
            url: x.url,
            type: 'district',
            khProvince: province.name,
            khDistrict: x.name
          };

          self._csvWriter.write(csv);
        })

        allDistricts = allDistricts.concat(districts);

        next();
      })
    }, function () {
      cb(null, allDistricts);
    });
  })
}

CambodiaPostcode.prototype.getAllKhCommunes = function (cb) {
  var self = this;

  self.getAllKhDistricts(function (err, districts) {
    if (err) return cb(err);

    var allCommunes = [];

    async.eachLimit(districts, 5, function (district, next) {
      self.logger.info(district.url)
      self.get(district.url, function (err, body) {
        if (err) {
          self.logger.error(err)
          return next();
        }

        var $ = cheerio.load(body);

        var communes = [];

        $('table tr').each(function (i) {
          if (i > 0) {
            var tds = $(this).find('td');

            communes.push({
              province: district.province,
              district: district.name,
              commune: toTitleCase(tds.first().text().trim().replace(/\r?\n|\r/g, '').trim()),
              postcode: tds.last().text().trim()
            });
          }
        });

        _.each(communes, function (x) {
          var csv = {
            id: x.id,
            url: x.url,
            type: 'commune',
            khProvince: x.province,
            khDistrict: x.district,
            khCommune: x.commune,
            postcode: x.postcode
          };

          self._csvWriter.write(csv);
        })

        allCommunes = allCommunes.concat(communes);
        next();
      })
    }, function () {
      cb(null, allCommunes)
    });
  })
}

CambodiaPostcode.prototype.getAllCommunes = function (cb) {

  var self = this;
  async.parallel([
    function (cb) { self.getAllEnProvinces(cb); }, //0
    function (cb) { self.getAllKhProvinces(cb); }, //1
    function (cb) { self.getAllEnDistricts(cb); }, //2
    function (cb) { self.getAllKhDistricts(cb); }, //3
    function (cb) { self.getAllEnCommunes(cb); },  //4
    function (cb) { self.getAllKhCommunes(cb); }   //5
  ], function (err, results) {
    if (err) {
      self.logger.error(err);
      cb(err)
    }
    var khProvinces = _.reduce(results[1], function (obj, param) {
      obj[param.id] = param;
      return obj;
    }, {});

    _.each(results[0], function (province) {
      self.csvWriter.write({
        id: province.id,
        url: province.url,
        type: 'province',
        enProvince: province.name,
        khProvince: khProvinces[province.id].name
      });
    });


    var khDistricts = _.reduce(results[3], function (obj, param) {
      obj[param.url.replace('/kh/', '/')] = param;

      return obj;
    }, {});

    // self.logger.info(_.map(results[2], function(x) { return x.url}))

    // return;
    _.each(results[2], function (district) {
      self.csvWriter.write({
        id: district.id,
        url: district.url,
        type: 'district',
        enProvince: district.province,
        khProvince: khDistricts[district.url].province,
        enDistrict: district.name,
        khDistrict: khDistricts[district.url].name
      });
    });


    var khCommunes = _.reduce(results[5], function (obj, param) {
      obj[param.postcode] = param;
      return obj;
    }, {});
    _.each(results[4], function (commune) {
      self.csvWriter.write({
        id: commune.id,
        url: commune.url,
        type: 'commune',
        enProvince: commune.province,
        enDistrict: commune.district,
        enCommune: commune.commune,
        khProvince: khCommunes[commune.postcode].province,
        khDistrict: khCommunes[commune.postcode].district,
        khCommune: khCommunes[commune.postcode].commune,
        postcode: commune.postcode
      });
    });

    cb(null, results);
  });
}

CambodiaPostcode.prototype.parseAllListingUrls = function (cb) {
  var self = this;

  var _communeUrls = [];

  self.getUrl('http://www.cambodiapostcode.com/features.html', function (err, body) {
    if (err) { returncb(err, _communeUrls); }


  });
}

CambodiaPostcode.prototype.parseAllListingUrlsByPage = function ($, cb) {
  var self = this;
  var _urls = $('a.btn.btn-success.btn-block').map(function () { return urljoin(self.baseUrl, $(this).attr('href')); }).toArray();

  return cb(null, _urls);
}


CambodiaPostcode.prototype.parseListingByUrl = function (listing, $, cb) {
  var self = this;
  trycatch(function () {
    // listing.commune = $('table tr').map(function() { var tds = $(this).find('td'); console.log(tds.first().text().trim()) });    

    cb(null, listing);
  }, function (err) {
    self.logger.error(err);
    cb(err);
  });
}

module.exports = CambodiaPostcode;