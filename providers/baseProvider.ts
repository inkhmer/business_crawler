// declare var require: {
//     <T>(path: string): T;
//     (paths: string[], callback: (...modules: any[]) => void): void;
//     ensure: (paths: string[], callback: (require: <T>(path: string) => T) => void) => void;
// };

var Sitemapper = require('sitemapper'),
    timespan = require('timespan'),
    levelup = require('level'),
    ttl = require('level-ttl'),
    config = require('config'),
    request = require('request'),
    cheerio = require('cheerio'),
    async = require('async'),
    _ = require('lodash'),
    urljoin = require('url-join'),
    jsutil = require('../libs/jsutil'),
    path = require('path'),
    fse = require('fs-extra'),
    slugify = require('slugify'),
    prettyHrtime = require('pretty-hrtime'),
    cloudscraper = require('cloudscraper'),
    Mapper = require('../libs/mapper'),
    winston = require('winston'),
    csvWriter = require('csv-write-stream'),
    trycatch = require('trycatch'),
    torReset = require('../libs/torReset'),
    hash = require('object-hash'),
    moment = require('moment');

import Listing = require('../models/listing');

class Provider {
    public name: string;
    public baseUrl: string;
    public options: any;
    public disabled: boolean = false;
    public proxyEnabled: boolean = false;
    public took: string;
    protected mapper: any;
    protected logger: any;
    protected listingProcessingQueue;
    protected _processedListingUrls: string[];

    public defaultCacheTime: any = {
        defaultTtl: timespan.fromDays(365).msecs,
        cachedRequest: timespan.fromDays(365).msecs,
        listingListPage: timespan.fromDays(365).msecs // cache 1 month
    };

    constructor(name: string, baseUrl: string, opts?: Object) {
        this.name = name;
        this.baseUrl = baseUrl;
        this._processedListingUrls = [];
        this.options = _.extend({
            sitemapFilename: 'sitemap.xml'
        }, opts);
        this.proxyEnabled = this.options.proxyEnabled || false;
        this.init();
    }

    private diskCache: any;

    private init(): void {
        var self = this,
            date = moment();

        var logFolder = path.resolve('logs/' + self.name, date.format('YYYY_MM_DD'));

        fse.ensureDirSync(logFolder);

        self.logger = new (winston.Logger)({
            exitOnError: false,
            transports: [
                new (winston.transports.Console)({
                    colorize: true,
                    prettyPrint: true
                }),
                new (winston.transports.File)({
                    name: 'info-file',
                    json: false,
                    filename: logFolder + date.format('/hh_mm_ss') + '-info.log',
                    level: 'info'
                }),
                new (winston.transports.File)({
                    name: 'error-file',
                    json: false,
                    filename: logFolder + date.format('/hh_mm_ss') + '-error.log',
                    level: 'error'
                })
            ]
        });

        /* 
        * Caching
        */
        var diskCacheDir = path.resolve('cache/leveldb');

        fse.ensureDirSync(path.resolve(diskCacheDir, 'parsed'));
        fse.ensureDirSync(path.resolve(diskCacheDir, 'request'));
        fse.ensureDirSync(path.resolve("data/" + self.name));
        this.diskCache = {
            _parseDb: ttl(levelup(path.resolve(diskCacheDir, 'parsed', this.name + '.db'))),
            _requestDb: ttl(levelup(path.resolve(diskCacheDir, 'request', this.name + '.db'))),
            _fileDb: ttl(levelup(path.resolve(diskCacheDir, 'file', this.name + '.db'))),
            wrap: function (key, result, cb) { self.dbWrap(this._parseDb, key, result, cb); },
            del: function (key, cb) {
                this._parseDb.del(key);
                this._requestDb.del(key);
            }
        };

        this.mapper = new Mapper(this.name);
    }

    private dbWrap(db: any, key: string, result: any, cb: (error: Error, data?: any) => void): void {
        // if (db.location.indexOf('parsed') > -1) {
        //     return result(cb);
        // }
        var self = this;
        db.get(key, function (err, data) {
            if (!err && data) {
                return cb(null, JSON.parse(data));
            }

            trycatch(function () {
                result(function (err, data) {
                    if (err) return cb(err);

                    db.put(key, JSON.stringify(data), {
                        ttl: self.defaultCacheTime.defaultTtl
                    }, function (err) {
                        if (err) {
                            self.logger.error(key, err);
                        }
                    });

                    return cb(null, data);
                });
            }, cb);
        });
    }

    private post(url: string, body: object, cb: (error: Error, data?: any) => void): void {
        var self = this;
        var requestOpts = {
            method: 'POST',
            url: url,
            headers: {
                'User-Agent': 'Mozilla/5.0 (compatible; YandexDirect/3.0; +http://yandex.com/bots)'
                // 'Accept-Encoding': 'gzip, deflate'
            },
        };

        var key = url;

        if (_.isFunction(body)) {
            cb = body;
        } else {
            requestOpts.json = body;

            key += hash(body);
        }
        if (this.proxyEnabled) {
            var agent;

            switch (this.name) {
                default:
                    requestOpts = _.extend(requestOpts, {
                        agentClass: agent,
                        agentOptions: {
                            socksHost: 'localhost', // Defaults to 'localhost'.
                            socksPort: 9050 // Defaults to 1080.
                        }
                    });
            };
        }


        this.dbWrap(this.diskCache._requestDb, key, function (cacheCallback) {
            request(requestOpts, function (error, response, body) {
                if (error) { return cacheCallback(error); }

                if (!body) return cacheCallback(new Error('No Data'));

                return cacheCallback(null, body);
            });
        }, cb);
    }

    private get(url: string, cb: (error: Error, data?: any) => void): void {
        var self = this;
        var requestOpts = {
            method: 'GET',
            url: url,
            headers: {
                'User-Agent': 'Mozilla/5.0 (compatible; YandexDirect/3.0; +http://yandex.com/bots)'
            },
        };

        var agent;

        if (url.indexOf('https') > -1) {
            agent = require('socks5-https-client/lib/Agent');
        } else {
            agent = require('socks5-http-client/lib/Agent');
        }
        if (this.proxyEnabled) {
            switch (this.name) {
                // case 'realestate':
                //     requestOpts = _.extend(requestOpts, {
                //         // proxy: 'http://111.90.179.42:8080'
                //          proxy: 'http://175.100.19.182:8080'	
                //     });
                //     break;

                default:
                    requestOpts = _.extend(requestOpts, {
                        agentClass: agent,
                        agentOptions: {
                            socksHost: 'localhost', // Defaults to 'localhost'.
                            socksPort: 9150 // Defaults to 1080.
                        }
                    });
            };
        }

        this.dbWrap(this.diskCache._requestDb, url, function (cacheCallback) {

            request(requestOpts, function (error, response, body) {
                if (error) { return cacheCallback(error); }

                if (!body) return cacheCallback(new Error('No Data'));
                if (body.indexOf('id="suspended"') > -1) {
                    return torReset.newIdentity(function () {

                        request(requestOpts, function (error, response, body) {
                            if (error) { return cacheCallback(error); }
                            if (!body) return cacheCallback(new Error('No Data'));
                            setTimeout(function () {
                                return cacheCallback(null, body);
                            }, self.options.timeout || 0);
                        });
                    });
                }
                setTimeout(function () {
                    return cacheCallback(null, body);
                }, self.options.timeout || 0);
            });

        }, cb);
    }

    public getSitemapUrls(cb: (error: Error, data?: any) => void): void {
        cb = _.isFunction(cb) ? cb : _.noop;
        var self = this;

        var sitemapFilename = self.options.sitemapFilename;

        self.diskCache.wrap(self.name + ':' + sitemapFilename, function (cacheCallback) {
            var sitemap = new Sitemapper({
                url: urljoin(self.baseUrl, sitemapFilename),
                timeout: timespan.fromHours(1).msecs //1 hour
            });

            sitemap.fetch().then(function (site) {
                cacheCallback(null, site.sites);
            }).catch(cacheCallback);

        }, cb);
    }

    public parseAllListingUrls(cb: (error: Error, data?: any) => void): void {
        cb = _.isFunction(cb) ? cb : _.noop;

        cb(null, []);
    }

    public parseListingByUrl(listing, $, cb: (error: Error, data?: any) => void): void {
        cb = _.isFunction(cb) ? cb : _.noop;

        cb(null, listing);
    }

    public getListingByUrl(url: string, cb: (error: Error, data?: any) => void): void {
        cb = _.isFunction(cb) ? cb : _.noop;
        var self = this;
        var providerName = this.name;

        self.diskCache.wrap(url, function (cacheCallback) {
            self.get(url, function (error, body) {
                if (error) return cb(error);

                var listing = new Listing(url, providerName);

                trycatch(function () {
                    var $ = cheerio.load(body);
                    self.logger.info('parsing', listing.url);

                    self.parseListingByUrl(listing, $, function (err, data) {
                        if (!data.companyName) self.diskCache.del(url);
                        cacheCallback(err, data);
                    });

                }, function (err) {
                    return cacheCallback(err);
                });
            });

        }, cb);
    }

    public parseAllListingUrlsByPage($: any, cb: (error: Error, data?: any) => void): void {
        cb = _.isFunction(cb) ? cb : _.noop;

        cb(null, []);
    }

    public getAllListingUrlsByPage(url: string, cb: (error: Error, data?: any) => void): void {
        cb = _.isFunction(cb) ? cb : _.noop;
        var self = this;

        self.get(url, function (error, body) {
            if (error) return cb(error);

            trycatch(function () {
                var $ = cheerio.load(body);

                self.parseAllListingUrlsByPage($, cb);
            }, cb);
        });
    }

    public getAllListingUrls(cb: (error: Error, data?: any) => void): void {
        cb = _.isFunction(cb) ? cb : _.noop;
        var self = this;
        var providerName = this.name;

        var cacheKey = providerName + ":getAllListingUrls";


        self.parseAllListingUrls(function (err, urls) {
            if (err) return cb(err);
            cb(null, _.uniq(urls));
        });

        // self.diskCache.wrap(cacheKey, function (cacheCallback) {
        //     self.parseAllListingUrls(function (err, urls) {
        //         if (err) return cacheCallback(err);
        //         cacheCallback(null, _.uniq(urls));
        //     });
        // }, cb);
    }

    public processListingUrl(url: string): void {
        var self = this;
        if (self._processedListingUrls.indexOf(url) == -1) {
            self._processedListingUrls.push(url);
            self.listingProcessingQueue.push(url);


            if (!self.options._lisingUrlCsvWriter) {
                self.options._lisingUrlCsvWriter = csvWriter({ headers: ['url'] });

                var fileName = path.resolve("data/" + self.name + "/listing-urls-x.csv");
                self.options._lisingUrlCsvWriter.pipe(fse.createWriteStream(fileName))
            }

            self.options._lisingUrlCsvWriter.write({ url: url });
        }
    }

    public processListingUrls(urls: string[]): void {
        var self = this;
        _.each(urls, function (url) {
            self.processListingUrl(url);
        })
    }

    public getAllListings(cb: (error: Error, data?: any) => void): void {
        cb = _.isFunction(cb) ? cb : _.noop;
        var self = this;
        var start = process.hrtime();

        var _listings = [];
        var _csvWriter = null;

        var c = 0;
        self.listingProcessingQueue = async.queue(function (url: string, next: () => void) {
            async.until(function () {
                return self.options.hasFinishedGrabListing;
            }, function (checkFn) {
                setTimeout(checkFn, timespan.fromMinutes(1).msecs)
            }, function () {
                self.getListingByUrl(url, function (err, listing) {
                    if (err) {
                        self.logger.info(err);
                        return next();
                    }

                    if (!_csvWriter) {
                        _csvWriter = csvWriter({ headers: _.keys(listing) });

                        var fileName = path.resolve("data/" + self.name + "/listing.csv");
                        _csvWriter.pipe(fse.createWriteStream(fileName))
                    }

                    _csvWriter.write(_.values(listing));

                    _listings.push(listing);

                    self.logger.info('provider', self.name, ' parsed ', listing.url, ++c, 'out of', self._processedListingUrls.length);

                    return next();
                });
            })
        }, 1);

        self.listingProcessingQueue.drain = function () {
            // _csvWriter.end()
            async.until(function () {
                return _.isFunction(self.options.allCb);
            }, function (checkFn) {
                setTimeout(checkFn, timespan.fromMinutes(1).msecs)
            }, self.options.allCb);
            // self.postProcess(_listings, function (err, listings) {
            //     var end = process.hrtime(start);
            //     self.took = prettyHrtime(end);

            //     cb(null, listings);
            // });
        }

        self.getAllListingUrls(function (err, urls) {
            if (err) return cb(err);

            var fileName = path.resolve("data/" + self.name + "/listing-urls.csv");

            fse.outputFileSync(fileName, (['url'].concat(urls)).join('\r\n'));

            self.logger.info('Provider', self.name, ' has total listings of', urls.length);

            self.processListingUrls(urls);
        });
    }

    public postProcess(listings: Listing[], cb: (error: Error, data?: any) => void): void {
        cb = _.isFunction(cb) ? cb : _.noop;

        var _listings = _.map(listings, function (x) {
            _.unset(x, 'setPropertyValue');
            _.unset(x, 'excludedCustomProperties')

            return x;
        });

        cb(null, _listings);
    }

    public listingNotExistChecker = function () {

    }
}

export = Provider;