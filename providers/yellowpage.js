var BaseProvider = require('./baseProvider'),
  async = require('async'),
  jsutil = require('../libs/jsutil'),
  trycatch = require('trycatch'),
  cheerio = require('cheerio'),
  numeral = require('numeral'),
  urljoin = require('url-join'),
   escapeHtml = require('escape-html'),
  parse = require('url-parse'),
  _ = require('lodash');

function YellowPage() {

}

function toTitleCase(str) {
  return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
}

var startUrls = [
  {
    url: 'https://www.yp.com.kh/search_results?page={{page}}&q=&location_value=Phnom+Penh%2C+Cambodia&city=Phnom+Penh&adm_lvl_1_sn=Phnom+Penh&country_sn=KH&location_type=locality&swlat=11.422401&nelat=11.7363275&swlng=104.71279609999999&nelng=105.0675774&lat=11.5448729&lng=104.89216680000004&faddress=Phnom+Penh%2C+Cambodia&place_id=ChIJ42tqxz1RCTERuyW1WugOAZw',
    totalPages: 4627
  }
];

YellowPage.prototype = new BaseProvider('YellowPage', 'https://www.yp.com.kh/');

YellowPage.prototype.parseAllListingUrls = function (cb) {
  var self = this;

  var _listingUrls = [];

  async.eachLimit(startUrls, 1, function (startUrl, nextType) {
    var listingListUrlTpl = _.template(startUrl.url);

    // var pages = _.range(1, 10); // _.range(1, startUrl.totalPages);
    var pages = _.range(1, startUrl.totalPages);
    async.eachLimit(pages, 5, function (i, next) {
      var nextUrl = listingListUrlTpl({ page: i });

      self.logger.info();
      self.logger.info('provider', self.name, ' processing ', nextUrl, i, 'out of', pages.length);

      self.getAllListingUrlsByPage(nextUrl, function (err, listingUrls) {
        if (err) {
          self.logger.error(err);
          return next();
        }

        self.processListingUrls(listingUrls);

        self.logger.info(nextUrl + ' has ', listingUrls.length, ' listings');
        if (listingUrls.length == 0) return next();
        _listingUrls = _.union(_listingUrls, listingUrls)
        next();
      });
    }, nextType);
  }, function () {
    cb(null, _listingUrls);
  })
}

YellowPage.prototype.parseAllListingUrlsByPage = function ($, cb) {
  var self = this;
  var _urls = $('a.btn.btn-success.btn-block').map(function () { return urljoin(self.baseUrl, $(this).attr('href')); }).toArray();

  return cb(null, _urls);
}


YellowPage.prototype.parseListingByUrl = function (listing, $, cb) {
  var self = this;
  trycatch(function () {
    listing.companyName = $('span[itemprop=name]').text().trim();
    listing.website = $('a[itemprop=url]').text().trim();
    listing.telephone = $('span[itemprop=telephone] div').text().trim();
    listing.emails = _.compact($('li:contains(Email)').next().text().trim().split(' ').map(function (x) {
      return x.replace(/(\n|\r)+$/, '');
    })).join(',');

    listing.addressStreet = $('span[itemprop=streetAddress]').text().trim();
    listing.addressLocality = $('span[itemprop=addressLocality]').text().trim();
    listing.addressPostalCode = $('span[itemprop=postalCode]').text().trim();

    var googleMapUrl = $('a[href*="maps.google.com"]').attr('href');

    if (googleMapUrl) {
      var googleUrl = parse(googleMapUrl, true);

      // https://maps.google.com/maps?ll=11.540894,104.891199&z=15&hl=en&gl=US&mapclient=apiv3
      var splits = googleUrl.query.daddr.split(',');
      if (splits.length == 2) {
        listing.addressLat = splits[0];
        listing.addressLng = splits[1];
      }
    }

    listing.addressLat = listing.addressLat || '';
    listing.addressLng = listing.addressLng || '';

    listing.socialFacebook = $('li.col-sm-8 a.facebook').attr('href');
    listing.socialFacebook = listing.socialFacebook || '';

    var cv = $('a[href*=curriculum]').attr('href');

    if (cv) {
      listing.cv = urljoin(self.baseUrl, cv);
    }
    listing.cv = listing.cv || '';

    var logo = $('.img-rounded').attr('src');
    if (logo) {
      listing.logo = urljoin(self.baseUrl, logo);
    }
    listing.logo = listing.logo || '';

    listing.tags = $('.tpad.tags.font-sm a').map(function () { return $(this).text(); }).toArray().join(',');

    // var description = $('span.xs-text-center').html() || $('ul.table-view.list-inline.xs-center-block').eq(1).html();

    // if (description) {
    //   listing.description = escapeHtml(description);
    // }

    _.each(_.keys(listing), function (x) {
      listing[x] = listing[x] || '';
    });

    cb(null, listing);
  }, function (err) {
    self.logger.error(err);
    cb(err);
  });
}

var yellowPage = new YellowPage();

yellowPage.disabled = false;

module.exports = yellowPage;