var BaseProvider = require('./baseProvider'),
  async = require('async'),
  jsutil = require('../libs/jsutil'),
  trycatch = require('trycatch'),
  cheerio = require('cheerio'),
  numeral = require('numeral'),
  urljoin = require('url-join'),
  escapeHtml = require('escape-html'),
  path = require('path'),
  fse = require('fs-extra'),
  // csvWriter = require('csv-write-stream'),
  parse = require('url-parse'),
  glob = require('glob'),
  _ = require('lodash');

function YellowPages(csvWriter) {
  this._csvWriter = csvWriter;
  this.workingFolderPath = path.resolve(__dirname + '/../data/yellowpages.com.au');
}

function toTitleCase(str) {
  return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); }).replace(/\s+/g, " ");
}

YellowPages.prototype = new BaseProvider('YellowPagesAu', 'https://www.yellowpages.com.au/');

YellowPages.prototype.getAllMainCategories = function (cb) {
  var self = this;

  return cb(null, ["Restaurants", "Doctors", "Dentists", "Mechanics", "Electricians", "Plumbers", "Hairdressers", "Solicitors", "Beauty Salons", "Builders", "Florists", "Pharmacies"])

  self.get(self.baseUrl, function (err, body) {
    if (err) {
      self.logger.error(err);
      return cb(err);
    }

    var $ = cheerio.load(body);
    var mainCategories = [];
    $('.rsNavItem.rsThumb .icon-title').each(function () {
      mainCategories.push($(this).text().trim());
    });


    cb(null, mainCategories);
  })
}

YellowPages.prototype.getFilteringByMainCategory = function (category, cb) {
  var self = this;
  var filePath = path.join(self.workingFolderPath, category + ' in All States, Australia.html');
  if (fse.existsSync(filePath)) {
    var body = fse.readFileSync(filePath);
    var $ = cheerio.load(body);
    var filters = [];
    $('.filter-cell').each(function () {
      var item = {
        name: $(this).find('.h3').text().trim(),
        items: []
      };

      $(this).find('.filter-items').each(function() {
        item.items.push($(this).text().trim())
      })
      item.items = _.sortBy(item.items);
      filters.push(item)
    })


    cb(null, filters);
  } else {
    cb(new Error('File doesn\'t exist: ' + filePath));
  }
}


YellowPages.prototype.getAllFilterings = function (cb) {
  var self = this;
  self.getAllMainCategories(function (err, categories) {
    var allFilterings = {};
    async.eachLimit(categories, 1, function (category, next) {
      self.getFilteringByMainCategory(category, function (err, filterings) {
        if (err) {
          next();
          return self.logger.error(err);
        }

        allFilterings[category] = filterings;
        next();
      })
    }, function () {
      cb(null, allFilterings)
    })
  });
}

module.exports = YellowPages;