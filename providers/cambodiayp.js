var BaseProvider = require('./baseProvider'),
  async = require('async'),
  jsutil = require('../libs/jsutil'),
  trycatch = require('trycatch'),
  cheerio = require('cheerio'),
  numeral = require('numeral'),
  urljoin = require('url-join'),
  escapeHtml = require('escape-html'),
  timespan = require('timespan'),
  parse = require('url-parse'),
  _ = require('lodash');

function CambodiaYP() {

}

function toTitleCase(str) {
  return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
}

var citieUrls = 'http://www.cambodiayp.com/browse-business-cities';

CambodiaYP.prototype = new BaseProvider('CambodiaYP', 'http://www.cambodiayp.com/', { proxyEnabled: true, timeout: timespan.fromMilliseconds(500).msecs });

CambodiaYP.prototype.parseAllListingUrls = function (cb) {
  var self = this;
  var _listingUrls = [];

  self.get(citieUrls, function (err, body) {
    if (err) {
      self.logger.error(err);
      return cb(err);
    } else {

      var $ = cheerio.load(body);

      var cities = [];

      $('.categories_list li>a').each(function () {
        cities.push({
          url: urljoin(self.baseUrl, $(this).attr('href')),
          text: $(this).text().trim(),
          total: parseInt($(this).next().text().replace(/,/g, '')),
          pages: Math.ceil(parseInt($(this).next().text().replace(/,/g, '')) / 20)
        })
      })

      var phnomPenh = cities.filter(x => x.text.indexOf('Phnom Penh') > -1);

   //   cities.reverse();

      async.eachLimit(cities, 1, function (city, nextCityFn) {
        self.logger.info('Processing ', city.url)

        self.get(city.url, function (err, body) {
          if (err) {
            self.logger.error(err);
            return nextCityFn();
          } else {
            var pageUrlTpl = _.template(city.url + '/{{page}}')

            if (city.pages > 1) {
              self.logger.info('Page no', city.pages)
              var pages = _.range(2, city.pages + 1);

              async.eachLimit(pages, 1, function (page, nextPageFn) {
                var pageUrl = pageUrlTpl({ page: page });

                self.logger.info('Processing ', pageUrl)

                self.getAllListingUrlsByPage(pageUrl, function (err, listingUrls) {
                  if (err) {
                    self.logger.error(err);
                    return nextPageFn();
                  }
                  if (listingUrls.length == 0) {
                    self.diskCache.del(pageUrl);
                    self.logger.warn(pageUrl, ' has no urls wait for next');
                  }
                  self.processListingUrls(listingUrls);

                  self.logger.info(pageUrl + ' has ', listingUrls.length, ' listings');
                  if (listingUrls.length == 0) return nextPageFn();
                  _listingUrls = _.union(_listingUrls, listingUrls)
                  nextPageFn();
                });
              }, nextCityFn);

            } else {
              self.getAllListingUrlsByPage(city.url, function (err, listingUrls) {
                if (err) {
                  self.logger.error(err);
                  return nextCityFn();
                }
                if (listingUrls.length == 0 && city.total > 0) {
                  self.diskCache.del(city.url);
                  self.logger.warn(city.url, ' has no urls wait for next');
                }
                self.processListingUrls(listingUrls);

                self.logger.info(city.url + ' has ', listingUrls.length, ' listings');
                if (listingUrls.length == 0) return nextCityFn();
                _listingUrls = _.union(_listingUrls, listingUrls)
                nextCityFn();
              });
            }
          }
        })
      }, function () {
        self.options.hasFinishedGrabListing = true;
        self.logger.info('finised city')
        self.options.allCb = function() {
          cb(null, _listingUrls);
        }
      })
    }
  })
}

CambodiaYP.prototype.parseAllListingUrlsByPage = function ($, cb) {
  var self = this;
  var _urls = $('.company h4 a').map(function () { return urljoin(self.baseUrl, $(this).attr('href')); }).toArray();
  return cb(null, _urls);
}


CambodiaYP.prototype.parseListingByUrl = function (listing, $, cb) {
  var self = this;
  
  trycatch(function () {

    listing.companyName = $('#company_name').text().trim();
    listing.website = $('.info .label:contains(Website)').next().find('a').attr('href');
    listing.telephone = _.compact([_.compact(($('.info .label:contains(Phone)').next().html() || '').split('<br>')).join('/'), _.compact(($('.info .label:contains(Mobile phone)').next().html() || '').split('<br>')).join('/')]).join('/');
    listing.emails = '';

    listing.addressStreet = $('.text.location').text().trim();
    // listing.addressLocality = $('span[itemprop=addressLocality]').text().trim();
    // listing.addressPostalCode = $('span[itemprop=postalCode]').text().trim();

    var googleMapUrl = $('#map_canvas').next().attr('href');

    if (googleMapUrl) {
      var googleUrl = parse(googleMapUrl, true);

      // https://maps.google.com/maps?ll=11.540894,104.891199&z=15&hl=en&gl=US&mapclient=apiv3
      var splits = googleUrl.query.daddr.split(',');
      if (splits.length == 2) {
        listing.addressLat = splits[0];
        listing.addressLng = splits[1];
      }
    }

    listing.addressLat = listing.addressLat || '';
    listing.addressLng = listing.addressLng || '';

    // listing.socialFacebook = $('li.col-sm-8 a.facebook').attr('href');
    // listing.socialFacebook = listing.socialFacebook || '';

    // var cv = $('a[href*=curriculum]').attr('href');

    // if (cv) {
    //   listing.cv = urljoin(self.baseUrl, cv);
    // }
    // listing.cv = listing.cv || '';

    // var logo = $('.img-rounded').attr('src');
    // if (logo) {
    //   listing.logo = urljoin(self.baseUrl, logo);
    // }
    // listing.logo = listing.logo || '';
    // listing.categories = categories
    listing.tags = $('.tags a').map(function () { return $(this).text(); }).toArray().join(',');

    // var description = $('span.xs-text-center').html() || $('ul.table-view.list-inline.xs-center-block').eq(1).html();

    // if (description) {
    //   listing.description = escapeHtml(description);
    // }

    _.each(_.keys(listing), function (x) {
      listing[x] = listing[x] || '';
    });

    cb(null, listing);
  }, function (err) {
    self.logger.error(err);
    cb(err);
  });
}

module.exports = CambodiaYP;