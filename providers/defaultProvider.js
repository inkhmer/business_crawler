var BaseProvider = require('./baseProvider'),
  async = require('async'),
  jsutil = require('../libs/jsutil'),
  trycatch = require('trycatch'),
  cheerio = require('cheerio'),
  numeral = require('numeral'),
  urljoin = require('url-join'),
  escapeHtml = require('escape-html'),
  // csvWriter = require('csv-write-stream'),
  parse = require('url-parse'),
  _ = require('lodash');

function DefaultProvider() {

}

DefaultProvider.prototype = new BaseProvider('DefaultProvider', '');

DefaultProvider.prototype.cache = function (key, executeCb, cb) {
  var self = this;

  self.dbWrap(self.diskCache._fileDb, key, executeCb, cb);
}

module.exports = DefaultProvider;