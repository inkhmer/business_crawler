"use strict";
var level = require('level'), sublevel = require('level-sublevel'), ttlLevel = require('level-ttl'), _ = require('lodash');
var db = {};
module.exports = function (dbName, ttl) {
    if (ttl === void 0) { ttl = true; }
    if (db[dbName])
        return db[dbName];
    if (ttl) {
        db[dbName] = ttlLevel(sublevel(level('./data/main.db')).sublevel(dbName));
    }
    else {
        db[dbName] = sublevel(level('./data/main.db')).sublevel(dbName);
    }
    db[dbName].getKeys = function (cb) {
        var keys = [];
        db[dbName].createKeyStream()
            .on('data', function (key) {
            // Get the actual key not the ttl;
            if (!_.startsWith(key, '!ttl')) {
                keys.push(key);
            }
        }).on('error', function (err) {
            cb(err);
        })
            .on('close', function () {
            // stream close
        })
            .on('end', function () {
            cb(null, keys);
        });
    };
    return db[dbName];
};
//# sourceMappingURL=db.js.map