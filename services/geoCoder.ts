import GeoLocation = require('../models/GeoLocation');
const NodeGeocoder = require('node-geocoder'),
    winston = require('../libs/winston');

const level = require('level')
    , config = require('config')
    , sublevel = require('level-sublevel')
    , timespan = require('timespan')
    , trycatch = require('trycatch')
    , ttl = require('level-ttl')
    , geoCodeDb = require('./db')('geocodes')
    , _ = require('lodash');

class GeoCoder {
    static GetFromAddress(address: string, cb: Function) {
        var self = this;
        var key = address.toLowerCase().trim();

        geoCodeDb.get(key, function (err, data) {
            if (!err && data) {
                return cb(null, JSON.parse(data));
            }

            trycatch(function () {
                var options = {
                    provider: 'google',
                    httpAdapter: 'https',
                    apiKey: config.get('apiKeys.google'),
                    formatter: null
                };

                var geocoder = NodeGeocoder(options);

                geocoder.geocode({
                    address: address, country: 'Cambodia'
                    // , minConfidence: 0.5
                    // , limit: 1 
                }, function (err, data) {
                    if (err) return cb(err);

                    geoCodeDb.put(key, JSON.stringify(data), { ttl: timespan.fromDays(365).msecs },
                        function (err) {
                            if (err) return winston.error(key, err);

                        })

                    return cb(null, data);
                });
            }, cb);
        })
    }
}

export = GeoCoder;