'use strict';
require('./libs/bootstrap');

var winston = require('./libs/winston'),
    async = require('async'),
    providers = require('./providers'),
    _ = require('lodash'),
    fse = require('fs-extra'),
    path = require('path'),
    glob = require("glob"),
    json2csv = require('json2csv');

/// With Memory Watch
var memwatch = require('memwatch-next');

memwatch.on('leak', function (info) {
    winston.error('Memory Leak', info);
});
// memwatch.on('stats', function (stats) {
//     winston.info('Memory Stats', stats);

// });

//node --expose-gc app.js
setInterval(function () {
    if (typeof gc === 'function') {
        gc();
            winston.info('Memory Usage', process.memoryUsage());
    }
}, 10000);

function updateDb() {

}

function merge(cb) {
    // options is optional
    glob("data/*.json", {}, function (err, files) {
        if (err) {
            winston.error(err);
            return cb();
        }

        var jsonFilename = path.resolve("data/tracks.json");

        var _tracks = [];

        async.each(files, function (file, next) {
            fse.readJson(file, function (err, listings) {
                _tracks = _tracks.concat(listings);

                next();
            });
        }, function () {
            winston.info();
            winston.info('Merging files');
            winston.info('File has been written saved to: ' + jsonFilename);

            var c = 0;
            _.each(_tracks, function (listing, idx) {
                if (!listing.name) {
                    c++;
                    if (listing.url) {
                        // winston.info(idx, listing.url);
                    } else {
                        //   winston.info(idx, listing);
                    }
                }

            });
            winston.info('Has no name: %s', c);
            fse.writeJsonSync(jsonFilename, _tracks);

            try {
                var fields = _.chain(_tracks[0]).keys().value();
                var csvFileName = path.resolve("data/tracks.csv");

                var result = json2csv({ data: _tracks, fields: fields });
                fse.outputFileSync(csvFileName, result);
                winston.info('File has been written saved to: ' + csvFileName);
            } catch (err) {
                winston.error(err);
            }

            //require('./convert-cms');
            cb();
        })
    });
}

// providers = [];//_.filter(providers, { 'name': 'realestate' });

async.eachLimit(providers, 5, function (provider, next) {
    var counter = _.after(4, next);

    provider.getAllListings(function (err, listings) {
        if (err) { return winston.error(err); }

        var folder = path.resolve(__dirname, 'data/' + provider.name);
        fse.ensureDirSync(folder);

        var jsonFilename = folder + '/listings.json';
        fse.writeJsonSync(jsonFilename, listings);

        winston.info();
        winston.info('Saving files for ' + provider.name);
        winston.info('File has been written saved to: ' + jsonFilename);

        winston.info();
        winston.info('It took', provider.took, 'to complete for', provider.name + '.');
        winston.info();

        try {
            var fields = _.chain(listings[0]).keys().value();
            var csvFileName = path.resolve("data/listings.csv");

            var result = json2csv({ data: listings, fields: fields });
            fse.outputFileSync(csvFileName, result);
            winston.info('File has been written saved to: ' + csvFileName);
        } catch (err) {
            winston.error(err);
        }

        counter();
    });



}, function () {
    winston.info('all items have been processed');
    process.exit();

    // merge(function () {
    //     process.exit();
    // });
});


//
// info.khmersong.today@gmail.com