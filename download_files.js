'use strict';
require('./libs/bootstrap');
/**
 * Module dependencies.
 */
// var app;

// var path = require('path');
// var app = require(path.resolve('./config/lib/app'));

// app.init(function () {
//   console.log('Initialized test automation');
// });

var winston = require('./libs/winston'),
    async = require('async'),
    trycatch = require('trycatch'),
    downloader = require('./libs/filedownloader'),
    urljoin = require('url-join'),
    fse = require('fs-extra'),
    path = require('path'),
    glob = require("glob"),
    rfs = require('rotating-file-stream'),
    moment = require('moment'),
    csvWriter = require('csv-write-stream'),
    _ = require('lodash');

const csv = require('csvtojson')
const csvPath = "D:\\Projects\\NodeJs\\business_crawler\\data\\v0.1\\listing.csv";
var c = 0;

var _csvWriter = null;


function cleanFiles(opt, fileFormat, retentionNumber) {
    fileFormat = "YYYY-MM-DD_hhmmss";
    retentionNumber = 2;

    glob(path.resolve("data/logos*.csv"), {}, function (err, files) {
        files = _.sortBy(files, function (file) {
            var fileName = path.basename(file);
            return moment(fileName, fileFormat);
        });

        var toDeleteCount = files.length - retentionNumber;

        if (toDeleteCount > 0) {
            var toDeleteFiles = _.take(files, toDeleteCount);

            _.each(toDeleteFiles, function (file) {
                fse.removeSync(file);
            });
        }
    });
}

function writeToCsv(file) {
    if (!_csvWriter) {
        _csvWriter = csvWriter({ headers: _.keys(file) });;
        var fileName = _.template('logos-{{time}}.csv')({ time: moment().format('YYYY-MM-DD_hhmmss') });
        var filePath = path.resolve("data/" + fileName);

        _csvWriter.pipe(fse.createWriteStream(filePath));
    }
    _csvWriter.write(_.values(file));
}

var imageDownloadQueue = async.queue(function (listing, next) {
    var imageUrl = listing.logo;

    if (!imageUrl) {
        winston.warn(imageUrl, 'no image url skipped');
        next();
    } else {
        var rawImageUrl = imageUrl;
        if (imageUrl.indexOf('https://www.yp.com.kh/https') > -1) {
            imageUrl = imageUrl.substr('https://www.yp.com.kh/'.length);
        }

        trycatch(function () {
            downloader.downloadImageFile(imageUrl, { path: 'logo' }, function (err, file) {
                if (err) winston.error(err);
                else {
                    file.url = rawImageUrl;
                    writeToCsv(file); next();
                }


            })
        }, function (err) {
            winston.error(err);
            next();
        })
    }
}, 5);

imageDownloadQueue.drain = function () {
    winston.info('All logo images have been downloaded.');
    cleanFiles();
    // if (_csvWriter) {
    //     _csvWriter.end();
    // }
}

function downloadLogoImages() {
    csv({ noheader: false })
        .fromFile(csvPath)
        .on('json', (jsonObj) => {
            imageDownloadQueue.push(jsonObj);
        })
        .on('done', () => {
            winston.info('All logo images have been read.');

        })
}

downloadLogoImages();